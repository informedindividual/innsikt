package org.informedindividual;

import org.informedindividual.model.Filter;
import org.informedindividual.util.NetworkHelper;
import org.informedindividual.util.lazylist.ImageLoader;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class SettingsFilterDetailActvity extends Activity {
	public static final String FILTER = "filter";

	private ImageLoader imageLoader;

	private Filter filter;

	private TextView detailTextView;

	private Button readmoreTextView;

	private Button okButton;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_filter_detail);
		filter = getIntent().getExtras().getParcelable(FILTER);
		imageLoader = new ImageLoader(this);
		final ImageView filterImageView = (ImageView)findViewById(R.id.filter_image);
		if ( (filter.imageUrl != null) && !filter.imageUrl.trim().equalsIgnoreCase("")) {
			imageLoader.DisplayImage(filter.imageUrl, this, filterImageView);
		}
		detailTextView = (TextView)findViewById(R.id.detail_textview);
		final String detailText = filter.description + "\n";
		readmoreTextView = (Button)findViewById(R.id.readmore_textview);
		if ( (filter.url != null) && !filter.url.trim().equalsIgnoreCase("")) {
			readmoreTextView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(final View view) {
					final String completeUrl = NetworkHelper.getCompleteUrl(filter.url);
					final Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(completeUrl));
					startActivity(browserIntent);
				}
			});
		} else {
			readmoreTextView.setVisibility(View.GONE);
		}
		detailTextView.setText(detailText);
		okButton = (Button)findViewById(R.id.aboutOk);
		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				finish();
			}
		});
	}
}
