package org.informedindividual.zxing.result;

import android.app.Activity;
import com.google.zxing.Result;
import com.google.zxing.client.result.ParsedResult;
import com.google.zxing.client.result.ResultParser;

public final class ResultHandlerFactory {
	private ResultHandlerFactory() {
	}

	public static ResultHandler makeResultHandler(final Activity activity, final Result rawResult) {
		final ParsedResult result = parseResult(rawResult);
		// The TextResultHandler is the fallthrough for unsupported formats.
		return new TextResultHandler(activity, result, rawResult);
	}

	private static ParsedResult parseResult(final Result rawResult) {
		return ResultParser.parseResult(rawResult);
	}
}
