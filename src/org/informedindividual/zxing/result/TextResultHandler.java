package org.informedindividual.zxing.result;

import org.informedindividual.R;
import android.app.Activity;
import com.google.zxing.Result;
import com.google.zxing.client.result.ParsedResult;

/**
 * This class handles TextParsedResult as well as unknown formats. It's the fallback handler.
 */
public final class TextResultHandler extends ResultHandler {
	public TextResultHandler(final Activity activity, final ParsedResult result, final Result rawResult) {
		super(activity, result, rawResult);
	}

	@Override
	public int getDisplayTitle() {
		return R.string.result_text;
	}
}
