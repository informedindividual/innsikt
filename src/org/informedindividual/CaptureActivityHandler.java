package org.informedindividual;

import java.util.Vector;

import org.informedindividual.zxing.DecodeThread;
import org.informedindividual.zxing.ViewfinderResultPointCallback;
import org.informedindividual.zxing.camera.CameraManager;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

/**
 * This class handles all the messaging which comprises the state machine for capture.
 */
public final class CaptureActivityHandler extends Handler {
	private final CaptureActivity activity;

	private final DecodeThread decodeThread;

	private State state;

	private enum State {
		PREVIEW, SUCCESS, DONE
	}

	CaptureActivityHandler(final CaptureActivity activity, final Vector<BarcodeFormat> decodeFormats, final String characterSet) {
		this.activity = activity;
		decodeThread = new DecodeThread(activity, decodeFormats, characterSet, new ViewfinderResultPointCallback(activity.viewfinderView));
		decodeThread.start();
		state = State.SUCCESS;
		// Start ourselves capturing previews and decoding.
		CameraManager.get().startPreview();
		restartPreviewAndDecode();
	}

	@Override
	public void handleMessage(final Message message) {
		switch (message.what) {
			case R.id.restart_preview:
				restartPreviewAndDecode();
				break;
			case R.id.decode_succeeded:
				state = State.SUCCESS;
				final Bundle bundle = message.getData();
				final Bitmap barcode = bundle == null ? null : (Bitmap)bundle.getParcelable(DecodeThread.BARCODE_BITMAP);
				activity.handleDecode((Result)message.obj, barcode);
				break;
			case R.id.decode_failed:
				// We're decoding as fast as possible, so when one decode fails, start another.
				state = State.PREVIEW;
				CameraManager.get().requestPreviewFrame(decodeThread.getHandler(), R.id.decode);
				break;
			case R.id.return_scan_result:
				activity.setResult(Activity.RESULT_OK, (Intent)message.obj);
				activity.finish();
				break;
		}
	}

	public void quitSynchronously() {
		state = State.DONE;
		CameraManager.get().stopPreview();
		final Message quit = Message.obtain(decodeThread.getHandler(), R.id.quit);
		quit.sendToTarget();
		try {
			decodeThread.join();
		} catch (final InterruptedException e) {
			// continue
		}
		// Be absolutely sure we don't send any queued up messages
		removeMessages(R.id.decode_succeeded);
		removeMessages(R.id.decode_failed);
	}

	private void restartPreviewAndDecode() {
		if (state == State.SUCCESS) {
			try {
				state = State.PREVIEW;
				CameraManager.get().requestPreviewFrame(decodeThread.getHandler(), R.id.decode);
				//CameraManager.get().requestAutoFocus(this, R.id.auto_focus);
				activity.drawViewfinder();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
	}
}
