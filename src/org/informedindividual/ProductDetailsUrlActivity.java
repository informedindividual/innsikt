package org.informedindividual;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class ProductDetailsUrlActivity extends Activity {
	public static final String DETAILS_URL = "url";

	private String detailsUrl;

	private WebView webview;

	private Button closeButton;

	private Dialog loadingDialog;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_detail_webview);
		detailsUrl = getIntent().getStringExtra(DETAILS_URL);
		loadingDialog = new Dialog(this, R.style.FullHeightDialog);
		loadingDialog.setContentView(R.layout.loading_layout);
		loadingDialog.setCancelable(true);
		loadingDialog.show();
		initializeCloseButton();
		initializeWebView();
	}

	private void initializeWebView() {
		webview = (WebView)findViewById(R.id.webview);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.getSettings().setBuiltInZoomControls(true);
		webview.getSettings().setDefaultZoom(ZoomDensity.FAR);
		webview.setWebViewClient(new WebViewClient() {
			/*
			 * Don't want to open a default browser when clicking or redirecting.
			 */
			@Override
			public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
				view.loadUrl(url);
				return true;
			}

			@Override
			public void onPageFinished(final WebView view, final String url) {
				loadingDialog.dismiss();
			}
		});
		webview.loadUrl(detailsUrl);
	}

	private void initializeCloseButton() {
		closeButton = (Button)findViewById(R.id.close_btn);
		closeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View arg0) {
				finish();
			}
		});
	}
}
