package org.informedindividual;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.apache.http.HttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.informedindividual.data.ProductDatabaseManager;
import org.informedindividual.menu.ActionItem;
import org.informedindividual.menu.QuickAction;
import org.informedindividual.model.Product;
import org.informedindividual.util.InputUtil;
import org.informedindividual.util.NetworkHelper;
import org.informedindividual.util.XmlFileParser;
import org.informedindividual.util.lazylist.ImageLoader;
import org.informedindividual.zxing.BeepManager;
import org.informedindividual.zxing.FinishListener;
import org.informedindividual.zxing.InactivityTimer;
import org.informedindividual.zxing.ViewfinderView;
import org.informedindividual.zxing.camera.CameraManager;
import org.informedindividual.zxing.result.ResultHandler;
import org.informedindividual.zxing.result.ResultHandlerFactory;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;

public final class CaptureActivity extends Activity implements SurfaceHolder.Callback {
	private final String TAG = "II";

	private IIApplication iiApplication;
	
	public static String FILTERS_CHANGED_KEY = "filtersChanged";
	
	public static String BARCODE_SWITCH_TO_TEST = "4242";
	
	public static String BARCODE_SWITCH_TO_PROD = "4243";

	private final MessageManager messageManager = new MessageManager(this);

	private final InputUtil inputUtil = new InputUtil(this);

	private SharedPreferences iiSharedPreferences;

	private ProductDatabaseManager productDatabaseManager;

	private ProductCursorAdapter productCursorAdapter;

	private ProgressBar headerLoadingBar;

	private TextView headerLoadingText;
	
	private TextView appTitle;

	private View headerView;

	private LayoutParams headerLayoutParams;

	private LinearLayout loadingLayout;

	private ProgressBar loadingBar;

	private TextView loadingText;

	private ListView productListView;

	private TextView productListEmptyView;

	private Cursor productCursor;

	private final XmlFileParser xmlFileParser = new XmlFileParser();

	private final int MIN_FRAME_HEIGHT = 240;

	private final int MAX_FRAME_HEIGHT = 360;

	private static final Set<ResultMetadataType> DISPLAYABLE_METADATA_TYPES;

	private final int MENU_SETTINGS_ID = Menu.FIRST;

	private final int MENU_ENTER_BARCODE_ID = Menu.FIRST + 1;

	private final int MENU_EMPTY_LIST_ID = Menu.FIRST + 2;

	private final int MENU_ABOUT_ID = Menu.FIRST + 3;
	static {
		DISPLAYABLE_METADATA_TYPES = new HashSet<ResultMetadataType>(5);
		DISPLAYABLE_METADATA_TYPES.add(ResultMetadataType.ISSUE_NUMBER);
		DISPLAYABLE_METADATA_TYPES.add(ResultMetadataType.SUGGESTED_PRICE);
		DISPLAYABLE_METADATA_TYPES.add(ResultMetadataType.ERROR_CORRECTION_LEVEL);
		DISPLAYABLE_METADATA_TYPES.add(ResultMetadataType.POSSIBLE_COUNTRY);
	}

	public CaptureActivityHandler captureActivityHandler;

	public ViewfinderView viewfinderView;

	private LinearLayout productListLayout;

	private boolean hasSurface;

	private Vector<BarcodeFormat> decodeFormats;

	private String characterSet;

	private InactivityTimer inactivityTimer;

	private BeepManager beepManager;

	static int fetchingProductCount = 0;

	private final Handler handler = new Handler();
	
	public Handler getHandler() {
	    return captureActivityHandler;
	  }

	private final Runnable updateResults = new Runnable() {
		@Override
		public void run() {
			if (fetchingProductCount == 0) {
				hideLoadingView();
			}
			displayProducts();
		}
	};

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.capture);
		iiApplication = (IIApplication)this.getApplication();
		iiApplication.captureActivity = this;
		CameraManager.init(getApplication());
		//checkIfUserSignedIn();
		sendUserToSettingsPage();
		initializeGuiElements();
		hideLoadingView();
		createQuickActionMenu();		
		initializeProductList();
		captureActivityHandler = null;
		hasSurface = false;
		inactivityTimer = new InactivityTimer(this);
		beepManager = new BeepManager(this);
		//Keep application alive while scanning, prevent screen turn-off
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	@Override
	public void onContentChanged() {
		super.onContentChanged();
		productListView = (ListView)findViewById(android.R.id.list);
		productListEmptyView = (TextView)findViewById(android.R.id.empty);
		productListView.setEmptyView(productListEmptyView);
	}
	
	@Override
	protected void onStart(){
		super.onStart();		
	}

	@Override
	protected void onResume() {
		super.onResume();		
		setTextToIndicateTestOrProd();
		
		if(getIntent().getExtras() != null && getIntent().getExtras().get(FILTERS_CHANGED_KEY) != null){			
			if((Boolean) getIntent().getExtras().get(FILTERS_CHANGED_KEY)){			
				//Get a list of barcodes from the database and put them into a temporary list					
				productDatabaseManager.openWritableDatabase();
				productCursor = productDatabaseManager.fetchAllProducts();
				ArrayList<String> barcodes = new ArrayList<String>();
				while (productCursor.moveToNext()) {
					barcodes.add(productCursor.getString(1));
				}
				productCursor.close();				
				//Delete all existing barcodes
				productDatabaseManager.deleteAllProducts();
				productDatabaseManager.close();							

				showLoadingView();
				fetchProductsFromServer(barcodes);
					
				//reset
				getIntent().putExtra(CaptureActivity.FILTERS_CHANGED_KEY, false);
				getIntent().getExtras().clear();				
			}
		}	

		displayProducts();
		
		final SurfaceView surfaceView = (SurfaceView)findViewById(R.id.preview_view);
		final SurfaceHolder surfaceHolder = surfaceView.getHolder();
		if (hasSurface) {
			// The activity was paused but not stopped, so the surface still exists. Therefore
			// surfaceCreated() won't be called, so init the camera here.
			initCamera(surfaceHolder);
		} else {
			// Install the callback and wait for surfaceCreated() to init the camera.
			surfaceHolder.addCallback(this);
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}
		inactivityTimer.onResume();
	}

	private void setTextToIndicateTestOrProd() {
		if(Configuration.SERVER_BASE_URL.equals(Configuration.SERVER_BASE_URL_TEST)){
			appTitle.setText(getString(R.string.app_name_test_environment));
		} else if(Configuration.SERVER_BASE_URL.equals(Configuration.SERVER_BASE_URL_PROD)){
			appTitle.setText(getString(R.string.app_name));
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (captureActivityHandler != null) {
			captureActivityHandler.quitSynchronously();
			captureActivityHandler = null;
		}
		inactivityTimer.onPause();
		CameraManager.get().closeDriver();
	}

	@Override
	protected void onDestroy() {
		inactivityTimer.shutdown();
		//productCursorAdapter.imageLoader.stopThread();
		//productListView.setAdapter(null);
		super.onDestroy();
	}

	@Override
	public void surfaceChanged(final SurfaceHolder holder, final int format, final int width, final int height) {
	}

	@Override
	public void surfaceCreated(final SurfaceHolder holder) {
		if (!hasSurface) {
			hasSurface = true;
			initCamera(holder);
		}
	}

	@Override
	public void surfaceDestroyed(final SurfaceHolder holder) {
		hasSurface = false;
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final boolean result = super.onCreateOptionsMenu(menu);
		menu.add(0, MENU_SETTINGS_ID, 0, R.string.settings);
		menu.add(0, MENU_ENTER_BARCODE_ID, 0, R.string.enter_barcode);
		menu.add(0, MENU_ABOUT_ID, 0, R.string.about);
		menu.add(0, MENU_EMPTY_LIST_ID, 0, R.string.empty_product_list);
		return result;
	}

	@Override
	public boolean onMenuItemSelected(final int featureId, final MenuItem item) {
		switch (item.getItemId()) {
			case MENU_SETTINGS_ID:
				final Intent settingsIntent = new Intent(CaptureActivity.this, SettingsActivity.class);
				startActivity(settingsIntent);
				break;
			case MENU_ENTER_BARCODE_ID:
				showEnterBarcodeDialogWindow();
				break;
			case MENU_EMPTY_LIST_ID:
				emptyProductList();
				break;
			case MENU_ABOUT_ID:
				showAboutDialogWindow();
				break;
		}
		return super.onMenuItemSelected(featureId, item);
	}

	private void emptyProductList() {
		productDatabaseManager.openWritableDatabase();
		productDatabaseManager.deleteAllProducts();
		getProductCursorAndBindListView();
		productDatabaseManager.close();
	}

	public void deleteProductFromList(final long productId) {
		productDatabaseManager.openWritableDatabase();
		productDatabaseManager.deleteProduct(productId);
		getProductCursorAndBindListView();
		productDatabaseManager.close();
	}
	
	private void sendUserToSettingsPage(){
		//First time login on this device
		//userFirstTimeLogin is default true, and set to false when user clicks on OK button in the info dialog.
		iiSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		final boolean userFirstTimeLogin = iiSharedPreferences.getBoolean(IIPreferences.USER_FIRST_TIME_LOGIN, true);
		if (userFirstTimeLogin) {
			//Send user to settings page
			final Intent intent = new Intent(CaptureActivity.this, SettingsActivity.class);
			startActivity(intent);
			this.finish();// fix the camera exception when returned back from the settings page.
		} 
	}
	
	private void checkIfUserSignedIn() {
		iiSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		final boolean userSignedIn = iiSharedPreferences.getBoolean(IIPreferences.USER_SIGNED_IN, false);//default is false
		if (!userSignedIn) {
			final Intent intent = new Intent(CaptureActivity.this, LoginActivity.class);
			startActivity(intent);
			finish();
		} else {
			//When the session is timed out, try to automatically login again to create a new session
			final String loginUrl = iiSharedPreferences.getString(IIPreferences.USER_LOGIN_URL, "");
			final String email = iiSharedPreferences.getString(IIPreferences.USER_EMAIL, "");
			final String password = iiSharedPreferences.getString(IIPreferences.USER_PASSWORD, "");
			final List<BasicNameValuePair> loginParams = new ArrayList<BasicNameValuePair>();
			final BasicNameValuePair emailParam = new BasicNameValuePair("email", email);
			final BasicNameValuePair passwordParam = new BasicNameValuePair("password", password);
			loginParams.add(emailParam);
			loginParams.add(passwordParam);
			//
			if (!loginUrl.equalsIgnoreCase("")) {
				try {
					iiApplication.network = new NetworkHelper(this);//Create a fresh http client
					iiApplication.network.httpPost(loginUrl.replace("email=", "").replace("password=", ""), loginParams);
				} catch (final IOException e) {
					//If the user is logged out, and try to login from the login page
					//It will throw an exception here because the user tries to login twice.
					//messageManager.showLoginFailedToastMessage();
				}
			}
		}
	}

	private void initializeProductList() {
		productListLayout = (LinearLayout)findViewById(R.id.product_list_layout);
		final LayoutParams params = productListLayout.getLayoutParams();
		final WindowManager manager = (WindowManager)this.getSystemService(Context.WINDOW_SERVICE);
		final Display display = manager.getDefaultDisplay();
		int height = display.getHeight() * 1 / 4;
		if (height < MIN_FRAME_HEIGHT) {
			height = MIN_FRAME_HEIGHT;
		} else if (height > MAX_FRAME_HEIGHT) {
			height = MAX_FRAME_HEIGHT;
		}
		final int layoutHeight = display.getHeight() - height;
		params.height = layoutHeight - 116;//hardcoding 116 for the top layout
	}

	private void initializeGuiElements() {
		viewfinderView = (ViewfinderView)findViewById(R.id.viewfinder_view);
		viewfinderView.setCameraManager(CameraManager.get());
		productDatabaseManager = new ProductDatabaseManager(this);
		
		appTitle = (TextView)findViewById(R.id.app_title);
		loadingLayout = (LinearLayout)findViewById(R.id.loading_layout);
		loadingBar = (ProgressBar)findViewById(R.id.loading_bar);
		loadingText = (TextView)findViewById(R.id.loading_text);
		productListView = (ListView)findViewById(android.R.id.list);
		productListEmptyView = (TextView)findViewById(android.R.id.empty);
		final LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
		headerView = inflater.inflate(R.layout.loading_layout, productListView, false);
		headerLoadingBar = (ProgressBar)headerView.findViewById(R.id.loading_progress_bar);
		headerLoadingText = (TextView)headerView.findViewById(R.id.loading_message);
		productListView.addHeaderView(headerView);
		productListView.setHeaderDividersEnabled(true);
		productListView.setItemsCanFocus(true);
		productListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(final AdapterView<?> adapterView, final View view, final int arg2, final long arg3) {
				final Intent intent = new Intent(CaptureActivity.this, ProductDetailActivity.class);
				intent.putExtra(ProductDetailActivity.BARCODE, view.getTag().toString());
				startActivity(intent);
			}
		});
	}

	public void displayProducts() {
		productDatabaseManager.openReadableDatabase();
		getProductCursorAndBindListView();
		productDatabaseManager.close();
		//
		if (!productListView.getAdapter().isEmpty()) {
			loadingBar.setVisibility(View.GONE);
			loadingText.setVisibility(View.GONE);
			loadingLayout.setVisibility(View.GONE);
		}
	}

	public void getProductCursorAndBindListView() {
		productCursor = productDatabaseManager.fetchAllProducts();
		final String[] fieldFrom = new String[]{ProductDatabaseManager.KEY_NAME, ProductDatabaseManager.KEY_VENDOR};
		final int[] fieldTo = new int[]{R.id.product_name, R.id.product_vendor};
		// Now create an array adapter and set it to display using our row
		productCursorAdapter = new ProductCursorAdapter(this, R.layout.product_row, productCursor, fieldFrom, fieldTo);
		productListView.setAdapter(productCursorAdapter);
	}

	private void createQuickActionMenu() {
		final ActionItem settingsActionItem = new ActionItem();
		settingsActionItem.setTitle(getString(R.string.settings));
		final ActionItem enterBarcodeActionItem = new ActionItem();
		enterBarcodeActionItem.setTitle(getString(R.string.enter_barcode));
		final ActionItem emptyListActionItem = new ActionItem();
		emptyListActionItem.setTitle(getString(R.string.empty_product_list));
		final ActionItem aboutActionItem = new ActionItem();
		aboutActionItem.setTitle(getString(R.string.about));
		//
		final QuickAction quickAction = new QuickAction(this);
		quickAction.addActionItem(settingsActionItem);
		quickAction.addActionItem(enterBarcodeActionItem);
		quickAction.addActionItem(emptyListActionItem);
		quickAction.addActionItem(aboutActionItem);
		quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(final int position) {
				switch (position) {
					case 0:
						//Settings
						final Intent settingsIntent = new Intent(CaptureActivity.this, SettingsActivity.class);
						startActivity(settingsIntent);
						break;
					case 1:
						//Enter barcode
						showEnterBarcodeDialogWindow();
						break;
					case 2:
						//Clear product list
						emptyProductList();
						break;
					case 3:
						//About
						showAboutDialogWindow();
						break;
				}
			}
		});
		final Button quickActionMenuBtn = (Button)this.findViewById(R.id.quick_menu_btn);
		quickActionMenuBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				quickAction.show(view);
				quickAction.setAnimStyle(QuickAction.ANIM_AUTO);
			}
		});
	}

	private void showEnterBarcodeDialogWindow() {
		final Dialog dialog = new Dialog(CaptureActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(true);
		dialog.setContentView(R.layout.enter_barcode);
		//Set full dialog width
		final WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
		layoutParams.copyFrom(dialog.getWindow().getAttributes());
		layoutParams.width = WindowManager.LayoutParams.FILL_PARENT;
		layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
		dialog.getWindow().setAttributes(layoutParams);
		final EditText barcodeEditText = (EditText)dialog.findViewById(R.id.enter_barcode_edittxt);
		barcodeEditText.requestFocus();
		inputUtil.setIntegerInputFilter(barcodeEditText);
		final Runnable runnable = new Runnable() {
			@Override
			public void run() {
				//Open keypad ready for user to enter numbers
				final InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				if (inputMethodManager != null) {
					inputMethodManager.showSoftInput(barcodeEditText, 0);
				}
			}
		};
		barcodeEditText.postDelayed(runnable, 100);
		final Button lookupButton = (Button)dialog.findViewById(R.id.lookup);
		lookupButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				final String inputBarcode = barcodeEditText.getText().toString();
				if (!inputBarcode.trim().equalsIgnoreCase("")) {
					lookupProduct(inputBarcode);
					dialog.dismiss();
				} else {
					barcodeEditText.postDelayed(runnable, 100);
					messageManager.showToastMessageShort(getString(R.string.enter_barcode));
				}
			}
		});
		barcodeEditText.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(final TextView view, final int actionId, final KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					lookupButton.performClick();
				}
				return false;
			}
		});
		dialog.show();
	}

	private void showAboutDialogWindow() {
		final Dialog dialog = new Dialog(CaptureActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(true);
		dialog.setContentView(R.layout.about);
		final Button button = (Button)dialog.findViewById(R.id.aboutOk);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}
	
	private void showWelcomeToTestServerInfoWindow() {
		final Dialog dialog = new Dialog(CaptureActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(true);
		dialog.setContentView(R.layout.test_server_welcome_info);
		final Button button = (Button)dialog.findViewById(R.id.aboutOk);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	/**
	 * A valid barcode has been found, so give an indication of success and show the results.
	 * 
	 * @param rawResult The contents of the barcode.
	 * @param barcode A greyscale bitmap of the camera data which was decoded.
	 */
	public void handleDecode(final Result rawResult, final Bitmap barcode) {
		inactivityTimer.onActivity();
		final ResultHandler resultHandler = ResultHandlerFactory.makeResultHandler(this, rawResult);
		beepManager.playBeepSoundAndVibrate();
		lookupProduct(resultHandler.getDisplayContents().toString());
	}

	/**
	 * Start fetching product information from the server in a separate thread.
	 */
	private void fetchProductInfoFromServer(final String barcode) {
		//a new thread is created for each lookup.
		final Thread thread = new Thread() {
			@Override
			public void run() {
				//final String userBarcodeLookupUrl = iiSharedPreferences.getString(IIPreferences.USER_BARCODE_LOOKUP_URL, "");				
				final String userBarcodeLookupUrl = Configuration.getUserbarcodeLookupUrl();
				final String barcodeLookupUrl = userBarcodeLookupUrl + barcode + "&filters=" + iiSharedPreferences.getString(IIPreferences.USER_FILTERS, "");
				try {
					fetchingProductCount = fetchingProductCount + 1;
					final HttpResponse productXmlResponse = iiApplication.network.httpGet(barcodeLookupUrl);
					final Product product = xmlFileParser.parseProductOverviewXml(productXmlResponse);
					if (product.barcode == "") {
						//Create an unknown product result
						product.barcode = barcode.toString();
						product.name = getString(R.string.unknow_product);
						product.vendor = barcode.toString();
						product.imageUrl = ProductCursorAdapter.UNKNOWN_PRODUCT_IMAGE_URL;
						product.opinion = Integer.toString(ProductCursorAdapter.UNKNOWN_PRODUCT_NOT_RATED_SCORE);
					}
					productDatabaseManager.openWritableDatabase();
					productDatabaseManager.createProduct(product);
					productDatabaseManager.close();
					if (fetchingProductCount > 0) {
						fetchingProductCount = fetchingProductCount - 1;
					}
					handler.post(updateResults);
				} catch (final IOException e) {
					if (fetchingProductCount > 0) {
						fetchingProductCount = fetchingProductCount - 1;
					}
					if (!iiApplication.network.isOnline()) {
						messageManager.showNetworkErrorMessage(CaptureActivity.this);
					} else {
						messageManager.showProductLookupErrorMessage();
					}
				} catch (final XmlPullParserException e) {
					if (fetchingProductCount > 0) {
						fetchingProductCount = fetchingProductCount - 1;
					}
				}
			}
		};
		thread.start();
	}
	
	private void fetchProductsFromServer(final List<String> barcodes) {		
		final Thread thread = new Thread() {
			@Override
			public void run() {
				//final String userBarcodeLookupUrl = iiSharedPreferences.getString(IIPreferences.USER_BARCODE_LOOKUP_URL, "");				
				final String userBarcodeLookupUrl = Configuration.getUserbarcodeLookupUrl();				
				String barcodeString = barcodes.toString();
				String multipleBarcodes = barcodeString.substring(1, barcodeString.length() - 1).replace(", ", ",");				
				final String barcodeLookupUrl = userBarcodeLookupUrl + multipleBarcodes + "&filters=" + iiSharedPreferences.getString(IIPreferences.USER_FILTERS, "");
				try {
					fetchingProductCount = fetchingProductCount + 1;
					final HttpResponse productXmlResponse = iiApplication.network.httpGet(barcodeLookupUrl);
					final List<Product> products = xmlFileParser.parseProductsOverviewXml(productXmlResponse);
					productDatabaseManager.openWritableDatabase();
					for (Product product : products) {
						//Unknown product will be removed after updating!
						/*
						if (product.barcode == "") {
							//Create an unknown product result
							product.barcode = barcode.toString();
							product.name = getString(R.string.unknow_product);
							product.vendor = barcode.toString();
							product.imageUrl = ProductCursorAdapter.UNKNOWN_PRODUCT_IMAGE_URL;
							product.opinion = Integer.toString(ProductCursorAdapter.UNKNOWN_PRODUCT_NOT_RATED_SCORE);
						}
						*/						
						productDatabaseManager.createProduct(product);						
					}	
					productDatabaseManager.close();
										
					if (fetchingProductCount > 0) {
						fetchingProductCount = fetchingProductCount - 1;
					}
					handler.post(updateResults);
				} catch (final IOException e) {
					if (fetchingProductCount > 0) {
						fetchingProductCount = fetchingProductCount - 1;
					}
					if (!iiApplication.network.isOnline()) {
						messageManager.showNetworkErrorMessage(CaptureActivity.this);
					} else {
						messageManager.showProductLookupErrorMessage();
					}
				} catch (final XmlPullParserException e) {
					if (fetchingProductCount > 0) {
						fetchingProductCount = fetchingProductCount - 1;
					}
				}
			}
		};
		thread.start();
	}

	private void lookupProduct(final String barcode) {
		//Switch server 		
		if(barcode.equals(BARCODE_SWITCH_TO_TEST)){
			Configuration.SERVER_BASE_URL = Configuration.SERVER_BASE_URL_TEST;
			showWelcomeToTestServerInfoWindow();
		} else if(barcode.equals(BARCODE_SWITCH_TO_PROD)){
			Configuration.SERVER_BASE_URL = Configuration.SERVER_BASE_URL_PROD;
		} else{
			showLoadingView();
			captureActivityHandler.sendEmptyMessageDelayed(R.id.restart_preview, 1000);//delay to prevent scanning the same product again immediately.
			fetchProductInfoFromServer(barcode);// in another thread
		}
		setTextToIndicateTestOrProd();
	}

	private void hideLoadingView() {
		if (loadingLayout.getVisibility() == View.VISIBLE) {
			loadingBar.setVisibility(View.GONE);
			loadingText.setVisibility(View.GONE);
			loadingLayout.setVisibility(View.GONE);
		} else {
			headerLoadingBar.setVisibility(View.GONE);
			headerLoadingText.setVisibility(View.GONE);
			headerLayoutParams = headerView.getLayoutParams();
			headerLayoutParams.height = 0;
		}
	}

	private void showLoadingView() {
		if (productListEmptyView.getVisibility() == View.VISIBLE) {
			loadingLayout.setVisibility(View.VISIBLE);
			loadingBar.setVisibility(View.VISIBLE);
			loadingText.setVisibility(View.VISIBLE);
			productListEmptyView.setVisibility(View.GONE);
		} else {
			headerLoadingBar.setVisibility(View.VISIBLE);
			headerLoadingText.setVisibility(View.VISIBLE);
			productListView.setSelection(0);//scroll to top so that the loading view is visible
		}
	}

	private void initCamera(final SurfaceHolder surfaceHolder) {
		try {
			CameraManager.get().openDriver(surfaceHolder);
			// Creating the handler starts the preview, which can also throw a RuntimeException.
			if (captureActivityHandler == null) {
				captureActivityHandler = new CaptureActivityHandler(this, decodeFormats, characterSet);
			}
		} catch (final IOException ioe) {
			Log.w(TAG, ioe);
			displayFrameworkBugMessageAndExit();
		} catch (final RuntimeException e) {
			// Barcode Scanner has seen crashes in the wild of this variety:
			// java.?lang.?RuntimeException: Fail to connect to camera service
			Log.w(TAG, "Unexpected error initializating camera", e);
			displayFrameworkBugMessageAndExit();
		}
	}

	private void displayFrameworkBugMessageAndExit() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.app_name));
		builder.setMessage(getString(R.string.msg_camera_framework_bug));
		builder.setPositiveButton(R.string.ok, new FinishListener(this));
		builder.setOnCancelListener(new FinishListener(this));
		builder.show();
	}

	public void drawViewfinder() {
		viewfinderView.drawViewfinder();
	}

	/**
	 * Adapter to display scanned products in list.
	 */
	private class ProductCursorAdapter extends SimpleCursorAdapter {
		static final int UNKNOWN_PRODUCT_NOT_RATED_SCORE = -2;

		static final String UNKNOWN_PRODUCT_IMAGE_URL = "?";

		final CaptureActivity activity;

		final Context context;

		final Cursor cursor;

		final ImageLoader imageLoader;

		private ProductCursorAdapter(final Context context, final int layout, final Cursor cursor, final String[] from, final int[] to) {
			super(context, layout, cursor, from, to);
			this.cursor = cursor;
			this.context = context;
			this.activity = (CaptureActivity)context;
			imageLoader = new ImageLoader(activity.getApplicationContext());
		}

		@Override
		public View getView(final int position, View convertView, final ViewGroup parent) {
			if (convertView == null) {
				convertView = View.inflate(context, R.layout.product_row, null);
			}
			final View row = convertView;
			cursor.moveToPosition(position);
			row.setTag(cursor.getString(1));
			final ImageView productImage = (ImageView)convertView.findViewById(R.id.product_image);
			imageLoader.DisplayImage(cursor.getString(4), activity, productImage);
			final TextView productNameTextView = (TextView)convertView.findViewById(R.id.product_name);
			productNameTextView.setText(cursor.getString(2));
			final TextView productVendorTextView = (TextView)convertView.findViewById(R.id.product_vendor);
			productVendorTextView.setText(cursor.getString(3));
			setOpinionColorAndValue(convertView);
			final Button deleteButton = (Button)convertView.findViewById(R.id.delete_btn);
			deleteButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(final View view) {
					cursor.moveToPosition(position);//without this wrong _id will be selected
					activity.deleteProductFromList(cursor.getLong(0));
				}
			});
			return row;
		}

		private void setOpinionColorAndValue(final View convertView) {
			final int score = cursor.getInt(5);
			final LinearLayout scoreLayout = (LinearLayout)convertView.findViewById(R.id.scoreBackgroundLayout);
			if (score == UNKNOWN_PRODUCT_NOT_RATED_SCORE) {
				scoreLayout.setVisibility(View.INVISIBLE);
			} else {
				scoreLayout.setVisibility(View.VISIBLE);
			}
			final TextView scoreTextView = (TextView)convertView.findViewById(R.id.score_txtview);
			scoreTextView.setText(Integer.toString(score) + "%");
			if (score <= 30) {
				//0-30 is displayed as a red number in a red circle.
				scoreLayout.setBackgroundResource(R.drawable.circle_red);
				scoreTextView.setTextColor(context.getResources().getColor(R.color.red));
			} else if ( (score >= 31) && (score <= 70)) {
				//31-70 is displayed as a black number in a black circle.
				scoreLayout.setBackgroundResource(R.drawable.circle_black);
				scoreTextView.setTextColor(context.getResources().getColor(R.color.black));
			} else if ( (score >= 71) && (score <= 100)) {
				//71-100 is displayed as a green number in a green circle.
				scoreLayout.setBackgroundResource(R.drawable.circle_green);
				scoreTextView.setTextColor(context.getResources().getColor(R.color.green));
			}
		}
	}
}
