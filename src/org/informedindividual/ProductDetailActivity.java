package org.informedindividual;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.informedindividual.model.Ingredient;
import org.informedindividual.model.Opinion;
import org.informedindividual.model.Product;
import org.informedindividual.util.XmlFileParser;
import org.informedindividual.util.lazylist.ImageLoader;
import org.xmlpull.v1.XmlPullParserException;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ProductDetailActivity extends Activity {
	private SharedPreferences iiSharedPreferences;

	private TextView appTitle;
	
	private static final int UNKNOWN_PRODUCT_NOT_RATED_SCORE = -2;

	private final String UNIT_GRAMS = "grams";

	private final String UNIT_KCAL = "kcal";

	private final String ENERGI = "energy";

	private final String ENERGI_KCAL = "energy-kcal";

	private final String FAT = "fat";

	private final String PROTEIN = "protein";

	private final String CARBOHYDRATES = "carbohydrates";

	public static String BARCODE = "barcode";

	private IIApplication iiApplication;

	private LayoutInflater inflater;

	final XmlFileParser xmlFileParser = new XmlFileParser();

	private final MessageManager messageManager = new MessageManager(this);

	private Product product;

	private ImageLoader imageLoader;

	private LinearLayout productDetailLayout;

	private Dialog loadingDialog;

	private ImageView productImage;

	private TextView productNameTextView;

	private TextView productVendorTextView;
	
	private String barcode;

	private final Handler handler = new Handler();

	private final Runnable updateResults = new Runnable() {
		@Override
		public void run() {
			productImage.setVisibility(View.VISIBLE);
			if (product.barcode != null) {
				displayImageNameVendor();
			} else {
				displayNoNameNoVendor();
			}
			createIngredientsGui();
			createNutritionalContentGui();
			setOpinionTotalValueAndColor();
			if (product.opinions != null) {
				for (final Opinion opinion : product.opinions) {
					createOpinionGui(opinion);
				}
			}
			loadingDialog.dismiss();
		}
	};

	private void displayImageNameVendor() {
		imageLoader.DisplayImage(product.imageUrl, ProductDetailActivity.this, productImage);
		if (!product.name.equalsIgnoreCase("")) {
			productNameTextView.setText(product.name);
		} else {
			productNameTextView.setTextAppearance(getApplicationContext(), R.style.italicBoldText);
			productNameTextView.setText(getString(R.string.no_name_available));
		}
		if (!product.vendor.equalsIgnoreCase("")) {
			productVendorTextView.setText(getString(R.string.barcode) + ": " + product.barcode + "\n" + getString(R.string.vendor) + ": " + product.vendor);
		} else {
			productVendorTextView.setTextAppearance(getApplicationContext(), R.style.italicText);
			productVendorTextView.setText(getString(R.string.no_vendor_available));
		}
	}

	private void displayNoNameNoVendor() {
		productNameTextView.setTextAppearance(getApplicationContext(), R.style.italicBoldText);
		productVendorTextView.setTextAppearance(getApplicationContext(), R.style.italicText);
		productNameTextView.setText(getString(R.string.no_name_available));
		productVendorTextView.setText(getString(R.string.no_vendor_available));
	}

	private void fetchProductDetailDataFromServer() {
		final Thread thread = new Thread() {
			@Override
			public void run() {
				try {
					//Get product detail XML data
					//String userBarcodeLookupUrl = iiSharedPreferences.getString(IIPreferences.USER_BARCODE_LOOKUP_URL, "");
					String userBarcodeLookupUrl = Configuration.getUserbarcodeLookupUrl();
					userBarcodeLookupUrl = userBarcodeLookupUrl.replace("detail=", "detail=high");
					final String productDetailUrl = userBarcodeLookupUrl + barcode + "&filters=" + iiSharedPreferences.getString(IIPreferences.USER_FILTERS, "");
					//final long resStartMillis = System.currentTimeMillis();
					final HttpResponse productDetailXmlResponse = iiApplication.network.httpGetResponse(productDetailUrl, null);
					//final long resEndMillis = System.currentTimeMillis();
					//final long resMillisUsed = resEndMillis - resStartMillis;
					//Log.d("XML Time", resMillisUsed + " resp");
					//final long startMillis = System.currentTimeMillis();
					product = xmlFileParser.parseProductDetailXml(productDetailXmlResponse);
					//final long endMillis = System.currentTimeMillis();
					//final long millisUsed = endMillis - startMillis;
					//Log.d("XML Time", millisUsed + "");
					handler.post(updateResults);
				} catch (final IOException e) {
					messageManager.showNetworkErrorMessage(ProductDetailActivity.this);
				} catch (final XmlPullParserException e) {
					messageManager.showXmlParsingErrorMessage(ProductDetailActivity.this);
				}
			}
		};
		thread.start();
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_detail);
		inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		iiApplication = (IIApplication)this.getApplication();
		iiSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		barcode = getIntent().getStringExtra(BARCODE);
		imageLoader = new ImageLoader(this);
		
		appTitle = (TextView)findViewById(R.id.app_title);
		productDetailLayout = (LinearLayout)findViewById(R.id.product_detail_layout);
		productImage = (ImageView)findViewById(R.id.product_detail_image);
		productNameTextView = (TextView)findViewById(R.id.product_name);
		productVendorTextView = (TextView)findViewById(R.id.product_vendor);
		loadingDialog = new Dialog(this, R.style.FullHeightDialog);
		loadingDialog.setContentView(R.layout.loading_layout);
		loadingDialog.setCancelable(true);
		loadingDialog.show();
		fetchProductDetailDataFromServer();
	}

	@Override
	public void onResume() {
		super.onResume();
		setTextToIndicateTestOrProd();
	}

	private void setOpinionTotalValueAndColor() {
		final View opinionTotalView = inflater.inflate(R.layout.product_detail_opinion_total, null);
		final LinearLayout scoreLayout = (LinearLayout)opinionTotalView.findViewById(R.id.scoreBackgroundLayout);
		final TextView opinionTextView = (TextView)opinionTotalView.findViewById(R.id.opinion_total_txtview);
		if ( (product.opinion != null) && !product.opinion.trim().equalsIgnoreCase("NaN") && !product.opinion.trim().equalsIgnoreCase(Integer.toString(UNKNOWN_PRODUCT_NOT_RATED_SCORE))) {
			displayTotalOpinion(opinionTotalView, scoreLayout);
		} else {
			displayNoOpinion(opinionTotalView, scoreLayout, opinionTextView);
		}
		productDetailLayout.addView(opinionTotalView);
	}

	private void displayTotalOpinion(final View opinionTotalView, final LinearLayout scoreLayout) {
		int score = UNKNOWN_PRODUCT_NOT_RATED_SCORE;
		if (!product.opinion.equalsIgnoreCase("NaN")) {
			score = Integer.parseInt(product.opinion);
		}
		if (score == UNKNOWN_PRODUCT_NOT_RATED_SCORE) {
			scoreLayout.setVisibility(View.INVISIBLE);
		} else {
			scoreLayout.setVisibility(View.VISIBLE);
		}
		final TextView scoreTextView = (TextView)opinionTotalView.findViewById(R.id.score_txtview);
		scoreTextView.setText(product.opinion + "%");
		setOpinionTextAndCircleColor(score, scoreLayout, scoreTextView);
	}

	private void displayNoOpinion(final View opinionTotalView, final LinearLayout scoreLayout, final TextView opinionTextView) {
		final View dividerView = opinionTotalView.findViewById(R.id.opinion_total_divider_view);
		final TextView noOpinionTextView = (TextView)opinionTotalView.findViewById(R.id.no_opinion_txtview);
		opinionTextView.setText(getString(R.string.opinions));
		scoreLayout.setVisibility(View.GONE);
		dividerView.setVisibility(View.GONE);
		noOpinionTextView.setTextAppearance(getApplicationContext(), R.style.italicText);
		noOpinionTextView.setVisibility(View.VISIBLE);
	}

	private void setOpinionTextAndCircleColor(final int score, final LinearLayout scoreLayout, final TextView scoreTextView) {
		if (score <= 30) {
			//0-30 is displayed as a red number in a red circle.
			scoreLayout.setBackgroundResource(R.drawable.circle_red);
			scoreTextView.setTextColor(getResources().getColor(R.color.red));
		} else if ( (score >= 31) && (score <= 70)) {
			//31-70 is displayed as a black number in a black circle.
			scoreLayout.setBackgroundResource(R.drawable.circle_black);
			scoreTextView.setTextColor(getResources().getColor(R.color.black));
		} else if ( (score >= 71) && (score <= 100)) {
			//71-100 is displayed as a green number in a green circle.
			scoreLayout.setBackgroundResource(R.drawable.circle_green);
			scoreTextView.setTextColor(getResources().getColor(R.color.green));
		}
	}

	private void createIngredientsGui() {
		final View ingredientsView = inflater.inflate(R.layout.product_detail_ingredients, null);
		final TextView ingredientsTextview = (TextView)ingredientsView.findViewById(R.id.ingredients);
		final Button ingredientsDetailsButton = (Button)ingredientsView.findViewById(R.id.ingredients_details_btn);
		String ingredientText = "";
		ArrayList<Ingredient> ingredients = null;
		if ( (product.ingredients != null) && (product.ingredients.size() > 0)) {
			ingredients = product.ingredients;
		}
		if (ingredients == null) {
			//No ingredient
			ingredientText = getString(R.string.no_ingredients);
			ingredientsDetailsButton.setVisibility(View.GONE);
			ingredientsTextview.setTextAppearance(getApplicationContext(), R.style.italicText);
		} else if (ingredients.size() > 3) {
			for (int i = 0; i < 3; i++) {
				ingredientText = ingredientText + ingredients.get(i).name + "\n";
			}
			ingredientText = ingredientText + "...";
			ingredientsDetailsButton.setVisibility(View.VISIBLE);
			initializeIngredientsDetailsButton(ingredientsDetailsButton);
		} else {
			for (int i = 0; i < ingredients.size(); i++) {
				ingredientText = ingredientText + ingredients.get(i).name + "\n";
			}
			ingredientsDetailsButton.setVisibility(View.GONE);
		}
		ingredientsTextview.setText(ingredientText);
		productDetailLayout.addView(ingredientsView);
	}

	private void initializeIngredientsDetailsButton(final Button ingredientsDetailsButton) {
		ingredientsDetailsButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				final Dialog dialog = new Dialog(ProductDetailActivity.this);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setCancelable(true);
				dialog.setContentView(R.layout.product_detail_ingredients_more);
				final TextView ingredientsMoreTextView = (TextView)dialog.findViewById(R.id.ingredients_textview);
				String ingredientsText = "";
				for (final Ingredient ingredient : product.ingredients) {
					ingredientsText = ingredientsText + "\n" + "   " + ingredient.name;
				}
				ingredientsText = ingredientsText + "                                                                                  ";
				ingredientsMoreTextView.setText(ingredientsText);
				final Button button = (Button)dialog.findViewById(R.id.aboutOk);
				button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(final View view) {
						dialog.dismiss();
					}
				});
				dialog.show();
			}
		});
	}

	private void createNutritionalContentGui() {
		final View nutritionView = inflater.inflate(R.layout.product_detail_nutrition, null);
		final TextView nutritionTextview = (TextView)nutritionView.findViewById(R.id.nutrition_text);
		final TextView nutritionContentTextview = (TextView)nutritionView.findViewById(R.id.nutrition_content);
		final Button moreNutritionButton = (Button)nutritionView.findViewById(R.id.nutrition_details_btn);
		if (product.nutrients.size() > 0) {
			displayNutrition(nutritionTextview, nutritionContentTextview, moreNutritionButton);
		} else {
			displayNoNutrition(nutritionView);
		}
		productDetailLayout.addView(nutritionView);
	}

	private void displayNutrition(final TextView nutritionTextview, final TextView nutritionContentTextview, final Button moreNutritionButton) {
		final String nutritionText = titleCase(ENERGI) + "\n" + titleCase(FAT) + "\n" + titleCase(PROTEIN) + "\n" + titleCase(CARBOHYDRATES);
		final String nutritionContentText = ": " + product.nutrients.get(ENERGI_KCAL) + " " + UNIT_KCAL + "\n" + ": " + product.nutrients.get(FAT) + " "
		+ UNIT_GRAMS + "\n" + ": " + product.nutrients.get(PROTEIN) + " " + UNIT_GRAMS + "\n" + ": " + product.nutrients.get(CARBOHYDRATES) + " "
		+ UNIT_GRAMS;
		moreNutritionButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				final Dialog dialog = new Dialog(ProductDetailActivity.this);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setCancelable(true);
				dialog.setContentView(R.layout.product_detail_nutrition_more);
				final TextView nutritionMoreTextView = (TextView)dialog.findViewById(R.id.nutrition_textview);
				String nutritionText = "";
				for (final Map.Entry<String, String> entry : product.nutrients.entrySet()) {
					nutritionText = nutritionText + "\n" + "   " + entry.getKey() + " " + entry.getValue();
				}
				nutritionText = nutritionText + "                                                                                  ";
				nutritionMoreTextView.setText(nutritionText);
				final Button button = (Button)dialog.findViewById(R.id.aboutOk);
				button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(final View view) {
						dialog.dismiss();
					}
				});
				dialog.show();
			}
		});
		nutritionTextview.setText(nutritionText);
		nutritionContentTextview.setText(nutritionContentText);
	}

	private void displayNoNutrition(final View nutritionView) {
		final LinearLayout nutritionLayout = (LinearLayout)nutritionView.findViewById(R.id.nutrition_layout);
		final TextView noNutritionTextView = (TextView)nutritionView.findViewById(R.id.no_nutrition_txtview);
		final TextView nutritionTitleTextView = (TextView)nutritionView.findViewById(R.id.nutrition_title);
		final TextView per100gTitleTextView = (TextView)nutritionView.findViewById(R.id.per_100g_title);
		noNutritionTextView.setTextAppearance(getApplicationContext(), R.style.italicText);
		nutritionTitleTextView.setText("");
		noNutritionTextView.setVisibility(View.GONE);
		nutritionLayout.setVisibility(View.GONE);
		per100gTitleTextView.setVisibility(View.GONE);
	}

	private String titleCase(final String text) {
		return text.substring(0, 1).toUpperCase() + text.substring(1, text.length()).toLowerCase();
	}

	private void createOpinionGui(final Opinion opinion) {
		final View opinionView = inflater.inflate(R.layout.product_detail_opinion, null);
		final TextView opinionNameTextview = (TextView)opinionView.findViewById(R.id.opinion_name_textview);
		final TextView opinionScoreTargetTextview = (TextView)opinionView.findViewById(R.id.score_target_txtview);
		final TextView opinionScorePointTextview = (TextView)opinionView.findViewById(R.id.score_point_txtview);
		if (opinion.targetType.equalsIgnoreCase("Ingredient")) {
			//<name> rates the ingredient <target-name> at <score>%
			opinionNameTextview.setText(opinion.name + " " + getString(R.string.rates_ingredient));
			opinionScoreTargetTextview.setText(opinion.targetName);
		} else if (opinion.targetType.equalsIgnoreCase("Product")) {
			//<name> rates the product at <score>%
			opinionNameTextview.setText(opinion.name + " " + getString(R.string.rates));
			opinionScoreTargetTextview.setText(getString(R.string.the_product));
		} else if (opinion.targetType.equalsIgnoreCase("Company")) {
			//<name> rates the company <target-name> at <score>%
			opinionNameTextview.setText(opinion.name + " " + getString(R.string.rates_company));
			opinionScoreTargetTextview.setText(opinion.targetName);
		} else if (opinion.targetType.equalsIgnoreCase("Category")) {
			//<name> rates the product at <score>%
			opinionNameTextview.setText(opinion.name + " " + getString(R.string.rates));
			opinionScoreTargetTextview.setText(getString(R.string.the_product));
		} 
		opinionScorePointTextview.setText(getString(R.string.at) + " " + opinion.score + "%");
		int score = UNKNOWN_PRODUCT_NOT_RATED_SCORE;
		if (!product.opinion.equalsIgnoreCase("NaN")) {
			score = Integer.parseInt(product.opinion);
		}
		setOpinionTextColor(score, opinionScorePointTextview);
		//
		final Button detailButton = (Button)opinionView.findViewById(R.id.opinion_details_btn);
		detailButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				final Intent intent = new Intent(ProductDetailActivity.this, OpinionDetailActvity.class);
				intent.putExtra(OpinionDetailActvity.OPINION, opinion);
				startActivity(intent);
			}
		});
		productDetailLayout.addView(opinionView);
	}

	private void setOpinionTextColor(final int score, final TextView scoreTextView) {
		if (score <= 30) {
			//0-30 is displayed as a red number in a red circle.
			scoreTextView.setTextColor(getResources().getColor(R.color.red));
		} else if ( (score >= 31) && (score <= 70)) {
			//31-70 is displayed as a black number in a black circle.
			scoreTextView.setTextColor(getResources().getColor(R.color.black));
		} else if ( (score >= 71) && (score <= 100)) {
			//71-100 is displayed as a green number in a green circle.
			scoreTextView.setTextColor(getResources().getColor(R.color.green));
		}
	}
	
	private void setTextToIndicateTestOrProd() {
		if(Configuration.SERVER_BASE_URL.equals(Configuration.SERVER_BASE_URL_TEST)){
			appTitle.setText(getString(R.string.app_name_test_environment));
		} else if(Configuration.SERVER_BASE_URL.equals(Configuration.SERVER_BASE_URL_PROD)){
			appTitle.setText(getString(R.string.app_name));
		}
	}
}
