package org.informedindividual;

import org.informedindividual.model.Opinion;
import org.informedindividual.util.NetworkHelper;
import org.informedindividual.util.lazylist.ImageLoader;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class OpinionDetailActvity extends Activity {
	public static final String OPINION = "opinion";

	private ImageLoader imageLoader;

	private Opinion opinion;

	private TextView detailTextView;

	private Button readmoreTextView;

	private Button okButton;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_detail_opinion_detail);
		opinion = getIntent().getExtras().getParcelable(OPINION);
		imageLoader = new ImageLoader(this);
		final ImageView opinionImageView = (ImageView)findViewById(R.id.opinion_image);
		if ( (opinion.imageUrl != null) && !opinion.imageUrl.trim().equalsIgnoreCase("")) {
			imageLoader.DisplayImage(opinion.imageUrl, this, opinionImageView);
		}
		detailTextView = (TextView)findViewById(R.id.detail_textview);
		final String detailText = opinion.description + "\n";
		readmoreTextView = (Button)findViewById(R.id.readmore_textview);
		if ( (opinion.url != null) && !opinion.url.trim().equalsIgnoreCase("")) {
			readmoreTextView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(final View view) {
					final String completeUrl = NetworkHelper.getCompleteUrl(opinion.url);
					final Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(completeUrl));
					startActivity(browserIntent);
				}
			});
		} else {
			readmoreTextView.setVisibility(View.GONE);
		}
		detailTextView.setText(detailText);
		okButton = (Button)findViewById(R.id.aboutOk);
		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				finish();
			}
		});
	}
}
