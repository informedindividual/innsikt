package org.informedindividual;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.informedindividual.model.Bootstrap;
import org.informedindividual.model.User;
import org.informedindividual.util.NetworkHelper;
import org.informedindividual.util.XmlFileParser;
import org.xmlpull.v1.XmlPullParserException;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends Activity {
	private Context context;

	private IIApplication iiApplication;

	private Bootstrap bootstrap;

	private final MessageManager messageManager = new MessageManager(this);

	private final XmlFileParser xmlFileParser = new XmlFileParser();

	private SharedPreferences iiSharedPreferences;

	private EditText emailEditText;

	private EditText passwordEditText;

	private Button loginButton;

	private Button createUserButton;

	private TextView loaddingMessageTextView;

	private Dialog loadingDialog;

	private final Handler loginHandler = new Handler();

	private final Runnable updateResults = new Runnable() {
		@Override
		public void run() {
			loadingDialog.dismiss();
		}
	};

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		context = this;
		iiSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		iiApplication = (IIApplication)this.getApplication();
		emailEditText = (EditText)findViewById(R.id.email_edittxt);
		passwordEditText = (EditText)findViewById(R.id.password_edittxt);
	}

	@Override
	public void onResume() {
		super.onResume();
		//Check if user is signed in
		final boolean userSignedIn = iiSharedPreferences.getBoolean(IIPreferences.USER_SIGNED_IN, false);//default is false
		if (userSignedIn) {
			finish();
		}
		//Get bootstrap XML data
		try {
			final HttpResponse bootstrapXmlResponse = iiApplication.network.httpGet(Configuration.getBootstrapUrl());
			bootstrap = xmlFileParser.parseBootstrapXml(bootstrapXmlResponse);
			initializeLoginButton(bootstrap);
			initializeCreateUserButton(bootstrap);
		} catch (final IOException e) {
			messageManager.showNetworkErrorMessage(LoginActivity.this);
		} catch (final XmlPullParserException e) {
			messageManager.showXmlParsingErrorMessage(LoginActivity.this);
		}
		final String email = getIntent().getStringExtra(CreateUserActivity.EMAIL);
		final String password = getIntent().getStringExtra(CreateUserActivity.PASSWORD);
		if ( (email != null) && (password != null)) {
			emailEditText.setText(email);
			passwordEditText.setText(password);
			loginButton.performClick();
		}
	}

	private void initializeCreateUserButton(final Bootstrap bootstrap) {
		createUserButton = (Button)findViewById(R.id.create_user_btn);
		createUserButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				final Intent intent = new Intent(LoginActivity.this, CreateUserActivity.class);
				intent.putExtra(CreateUserActivity.EMAIL, emailEditText.getText().toString());
				intent.putExtra(CreateUserActivity.PASSWORD, passwordEditText.getText().toString());
				intent.putExtra(CreateUserActivity.EMAIL_EXISTS_URL, bootstrap.emailExistsUrl);
				intent.putExtra(CreateUserActivity.CREATE_USER_URL, bootstrap.createUserUrl);
				intent.putExtra(CreateUserActivity.USER_LOGIN_URL, bootstrap.loginUrl);
				intent.putParcelableArrayListExtra(CreateUserActivity.FILTERS, bootstrap.filters);
				startActivity(intent);
			}
		});
	}

	private void initializeLoginButton(final Bootstrap bootstrap) {
		loginButton = (Button)findViewById(R.id.login_btn);
		loginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				if (emailEditText.getText().toString().equalsIgnoreCase("")) {
					messageManager.showToastMessageShort(getString(R.string.email_required));
				} else if (passwordEditText.getText().toString().equalsIgnoreCase("")) {
					messageManager.showToastMessageShort(getString(R.string.password_required));
				} else {
					initializeLoggingInMessage();
					authenticating();
				}
			}
		});
	}

	private void initializeLoggingInMessage() {
		loadingDialog = new Dialog(LoginActivity.this, R.style.FullHeightDialog);
		loadingDialog.setContentView(R.layout.loading_layout);
		loaddingMessageTextView = (TextView)loadingDialog.findViewById(R.id.loading_message);
		loaddingMessageTextView.setText(R.string.login_in);
		loadingDialog.setCancelable(true);
		loadingDialog.show();
	}

	private void authenticating() {
		final Thread thread = new Thread() {
			@Override
			public void run() {
				final String loginUrl = bootstrap.loginUrl;
				final String email = emailEditText.getText().toString();
				final String password = passwordEditText.getText().toString();
				try {
					final List<BasicNameValuePair> loginParams = new ArrayList<BasicNameValuePair>();
					final BasicNameValuePair emailParam = new BasicNameValuePair("email", email);
					final BasicNameValuePair passwordParam = new BasicNameValuePair("password", password);
					loginParams.add(emailParam);
					loginParams.add(passwordParam);
					//
					iiApplication.network = new NetworkHelper(context);//Create a fresh http client
					final HttpResponse loginResultResponse = iiApplication.network.httpPost(loginUrl.replace("email=", "").replace("password=", ""),
							loginParams);
					if ( (loginResultResponse != null)) {
						final User user = xmlFileParser.parseUserLoginXml(loginResultResponse);
						final SharedPreferences.Editor iiPreferencesEditor = iiSharedPreferences.edit();
						iiPreferencesEditor.putBoolean(IIPreferences.USER_SIGNED_IN, true);
						iiPreferencesEditor.putString(IIPreferences.USER_KEY, user.userKey);
						iiPreferencesEditor.putString(IIPreferences.USER_NAME, user.name);
						iiPreferencesEditor.putString(IIPreferences.USER_EMAIL, email);
						iiPreferencesEditor.putString(IIPreferences.USER_PASSWORD, password);
						iiPreferencesEditor.putString(IIPreferences.USER_LOGIN_URL, loginUrl);
						iiPreferencesEditor.putString(IIPreferences.USER_LOGOUT_URL, user.logoutUrl);
						iiPreferencesEditor.putString(IIPreferences.USER_SETTINGS_URL, user.settingsUrl);
						iiPreferencesEditor.putString(IIPreferences.USER_BARCODE_LOOKUP_URL, user.barcodeLookupUrl);
						iiPreferencesEditor.commit();
						//First time login on this device
						//userFirstTimeLogin is default true, and set to false when user clicks on OK button in the info dialog.
						final boolean userFirstTimeLogin = iiSharedPreferences.getBoolean(IIPreferences.USER_FIRST_TIME_LOGIN, true);
						if (userFirstTimeLogin) {
							//Send user to settings page
							final Intent intent = new Intent(LoginActivity.this, SettingsActivity.class);
							startActivity(intent);
						} else {
							//Start capture activity
							final Intent intent = new Intent(LoginActivity.this, CaptureActivity.class);
							startActivity(intent);
						}
						finish();
					} else {
						//login failed
						messageManager.showLoginFailedToastMessage();
					}
				} catch (final Exception e) {
					//login failed, network error..
					messageManager.showLoginFailedToastMessage();
				}
				loginHandler.post(updateResults);
			}
		};
		thread.start();
	}
}
