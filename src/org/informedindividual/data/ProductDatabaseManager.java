package org.informedindividual.data;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.informedindividual.model.Product;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ProductDatabaseManager {
	private static final String DATABASE_NAME = "ii";

	private static final int DATABASE_VERSION = 1;

	public static final String TABLE_PRODUCT = "product";

	public static final String KEY_ID = "_id";

	public static final String KEY_BARCODE = "barcode";

	public static final String KEY_NAME = "name";

	public static final String KEY_VENDOR = "vendor";

	public static final String KEY_IMAGE_URL = "imageUrl";

	public static final String KEY_OPINION = "opinion";

	public static final String KEY_TIMESTAMP = "timestamp";

	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private final String CREATE_PRODUCT_TABLE_SQL = "CREATE TABLE " + TABLE_PRODUCT + "(_id INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_BARCODE
	+ " TEXT NOT NULL," + KEY_NAME + " TEXT NOT NULL," + KEY_IMAGE_URL + " TEXT NOT NULL," + KEY_OPINION + " INTEGER NOT NULL," + KEY_VENDOR
	+ " TEXT NOT NULL," + KEY_TIMESTAMP + " DATETIME NOT NULL);";

	private final String DROP_PRODUCT_TABLE_SQL = "DROP TABLE IF EXISTS " + TABLE_PRODUCT + ";";

	private SQLiteDatabase database;

	private final ProductDatabaseHelper productDatabaseHelper;

	public ProductDatabaseManager(final Context context) {
		productDatabaseHelper = new ProductDatabaseHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public ProductDatabaseManager openReadableDatabase() throws SQLException {
		database = productDatabaseHelper.getReadableDatabase();
		return this;
	}

	public ProductDatabaseManager openWritableDatabase() throws SQLException {
		database = productDatabaseHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		productDatabaseHelper.close();
	}

	private ContentValues createProductContentValues(final Product product) {
		final ContentValues values = new ContentValues();
		values.put(KEY_BARCODE, product.barcode);
		values.put(KEY_NAME, product.name);
		values.put(KEY_VENDOR, product.vendor);
		values.put(KEY_IMAGE_URL, product.imageUrl);
		values.put(KEY_OPINION, product.opinion);
		values.put(KEY_TIMESTAMP, dateFormat.format(new Date()));
		return values;
	}

	public long createProduct(final Product product) {
		openWritableDatabase();
		final ContentValues initialValues = createProductContentValues(product);
		return database.insert(TABLE_PRODUCT, null, initialValues);
	}

	public boolean updateProduct(final long rowId, final Product product) {
		final ContentValues updateValues = createProductContentValues(product);
		return database.update(TABLE_PRODUCT, updateValues, KEY_ID + "=" + rowId, null) > 0;
	}

	public boolean deleteProduct(final long rowId) {
		return database.delete(TABLE_PRODUCT, KEY_ID + "=" + rowId, null) > 0;
	}

	/**
	 * Return a cursor over the list of all products in the database.
	 */
	public Cursor fetchAllProducts() {
		return database.query(TABLE_PRODUCT, new String[]{KEY_ID, KEY_BARCODE, KEY_NAME, KEY_VENDOR, KEY_IMAGE_URL, KEY_OPINION, KEY_TIMESTAMP}, null, null,
				null, null, KEY_TIMESTAMP + " DESC");
	}

	public boolean deleteAllProducts() {
		return database.delete(TABLE_PRODUCT, null, null) > 0;
	}

	/**
	 * Return a cursor positioned at the defined product.
	 */
	public Cursor fetchProduct(final long rowId) throws SQLException {
		final Cursor cursor = database.query(true, TABLE_PRODUCT, new String[]{KEY_ID, KEY_NAME, KEY_VENDOR}, KEY_ID + "=" + rowId, null, null, null, null,
				null);
		if (cursor != null) {
			cursor.moveToFirst();
		}
		return cursor;
	}

	private class ProductDatabaseHelper extends SQLiteOpenHelper {
		/**
		 * The database is created when the constructor is called.
		 */
		public ProductDatabaseHelper(final Context context, final String name, final CursorFactory factory, final int version) {
			super(context, name, factory, version);
		}

		/**
		 * Method is called during creation of the database
		 */
		@Override
		public void onCreate(final SQLiteDatabase db) {
			db.execSQL(CREATE_PRODUCT_TABLE_SQL);
		}

		/**
		 * Method is called during an update of the database, e.g. if you increase the database version
		 */
		@Override
		public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
			Log.w(ProductDatabaseHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion
					+ ", which will destroy all old data");
			db.execSQL(DROP_PRODUCT_TABLE_SQL);
			onCreate(db);
		}
	}
}
