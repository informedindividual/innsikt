package org.informedindividual;

import org.informedindividual.util.MessageUtil;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;

public class MessageManager {
	private final Context context;

	private Activity messageOwner;

	private final MessageUtil messageUtil;

	private final int NETWORK_ERROR_MESSAGE = 1;

	private final int XML_PARSING_ERROR_MESSAGE = 2;

	private final int PRODUCT_NOT_FOUND_ERROR_MESSAGE = 3;

	private final int LOOKUP_PRODUCT_ERROR_MESSAGE = 4;

	private final int LOGIN_FAILED = 5;

	private final int CREATE_USER_FAILED = 6;

	public MessageManager(final Context context) {
		this.context = context;
		messageUtil = new MessageUtil(context);
	}

	private final Handler messageHandler = new Handler() {
		@Override
		public void handleMessage(final Message message) {
			switch (message.what) {
				case NETWORK_ERROR_MESSAGE:
					showErrorMessageDialog(messageOwner, context.getString(R.string.network_error));
					break;
				case XML_PARSING_ERROR_MESSAGE:
					showErrorMessageDialog(messageOwner, context.getString(R.string.xml_parsing_error));
					break;
				case PRODUCT_NOT_FOUND_ERROR_MESSAGE:
					showErrorMessageDialog(context.getString(R.string.product_not_found_error));
					break;
				case LOOKUP_PRODUCT_ERROR_MESSAGE:
					showErrorMessageDialog(context.getString(R.string.lookup_product_error));
					break;
				case LOGIN_FAILED:
					messageUtil.textToastShort(context.getString(R.string.login_failed));
					break;
				case CREATE_USER_FAILED:
					messageUtil.textToastShort(context.getString(R.string.create_user_failed));
					break;
				default:
					break;
			}
		}
	};

	public void showLoginFailedToastMessage() {
		messageHandler.sendMessage(Message.obtain(messageHandler, LOGIN_FAILED));
	}

	public void showCreateUserFailedToastMessage() {
		messageHandler.sendMessage(Message.obtain(messageHandler, CREATE_USER_FAILED));
	}

	public void showProductNotFoundErrorMessage() {
		messageHandler.sendMessage(Message.obtain(messageHandler, PRODUCT_NOT_FOUND_ERROR_MESSAGE));
	}

	public void showProductLookupErrorMessage() {
		messageHandler.sendMessage(Message.obtain(messageHandler, LOOKUP_PRODUCT_ERROR_MESSAGE));
	}

	public void showNetworkErrorMessage(final Activity activity) {
		messageOwner = activity;
		messageHandler.sendMessage(Message.obtain(messageHandler, NETWORK_ERROR_MESSAGE));
	}

	public void showXmlParsingErrorMessage(final Activity activity) {
		messageOwner = activity;
		messageHandler.sendMessage(Message.obtain(messageHandler, XML_PARSING_ERROR_MESSAGE));
	}

	public void showToastMessageShort(final String message) {
		messageUtil.textToastShort(message);
	}

	private void showErrorMessageDialog(final String message) {
		final AlertDialog dialog = new AlertDialog.Builder(context).create();
		dialog.setCancelable(false); // This blocks the 'BACK' button
		dialog.setMessage(message);
		dialog.setButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	/**
	 * Show error message, and close the activity when user clicks the OK button.
	 */
	private void showErrorMessageDialog(final Activity activity, final String message) {
		final AlertDialog dialog = new AlertDialog.Builder(context).create();
		dialog.setCancelable(false); // This blocks the 'BACK' button
		dialog.setMessage(message);
		dialog.setButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				dialog.dismiss();
				activity.finish();
			}
		});
		dialog.show();
	}
}
