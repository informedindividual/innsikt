package org.informedindividual;

public class IIPreferences {
	static final String USER_FIRST_TIME_LOGIN = "user_first_time_login";

	static final String USER_SIGNED_IN = "user_signed_in";

	static final String USER_EMAIL = "email";

	static final String USER_PASSWORD = "password";

	static final String USER_KEY = "key";

	static final String USER_NAME = "user_name";

	static final String USER_LOGIN_URL = "login_url";

	static final String USER_LOGOUT_URL = "logout_url";

	static final String USER_SETTINGS_URL = "settings_url";

	static final String USER_BARCODE_LOOKUP_URL = "barcode_lookup_url";

	static final String USER_FILTERS = "user_filters";
	
	//  
	public static final String KEY_DECODE_1D = "preferences_decode_1D";
	  public static final String KEY_DECODE_QR = "preferences_decode_QR";
	  public static final String KEY_DECODE_DATA_MATRIX = "preferences_decode_Data_Matrix";
	  public static final String KEY_CUSTOM_PRODUCT_SEARCH = "preferences_custom_product_search";

	  public static final String KEY_PLAY_BEEP = "preferences_play_beep";
	  public static final String KEY_VIBRATE = "preferences_vibrate";
	  public static final String KEY_COPY_TO_CLIPBOARD = "preferences_copy_to_clipboard";
	  public static final String KEY_FRONT_LIGHT = "preferences_front_light";
	  public static final String KEY_BULK_MODE = "preferences_bulk_mode";
	  public static final String KEY_REMEMBER_DUPLICATES = "preferences_remember_duplicates";
	  public static final String KEY_SUPPLEMENTAL = "preferences_supplemental";
	  public static final String KEY_AUTO_FOCUS = "preferences_auto_focus";
	  public static final String KEY_SEARCH_COUNTRY = "preferences_search_country";

	  public static final String KEY_DISABLE_CONTINUOUS_FOCUS = "preferences_disable_continuous_focus";
	  //public static final String KEY_DISABLE_EXPOSURE = "preferences_disable_exposure";

	  public static final String KEY_HELP_VERSION_SHOWN = "preferences_help_version_shown";
}
