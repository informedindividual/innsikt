package org.informedindividual.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class IOUtil {
	// Fast Implementation
	public static StringBuilder inputStreamToString(final InputStream is) throws IOException {
		String line = "";
		final StringBuilder total = new StringBuilder();
		// Wrap a BufferedReader around the InputStream
		final BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		// Read response until the end
		while ( (line = rd.readLine()) != null) {
			total.append(line);
		}
		// Return full string
		return total;
	}
}
