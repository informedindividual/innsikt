package org.informedindividual.util.lazylist;

import java.io.File;
import android.content.Context;

public class FileCache {
	private final String cacheDirName = ".InformedIndividual";

	private File cacheDir;

	public FileCache(final Context context) {
		//Find the directory to save cached images
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
			cacheDir = new File(android.os.Environment.getExternalStorageDirectory(), cacheDirName);
		} else {
			cacheDir = context.getCacheDir();
		}
		if (!cacheDir.exists()) {
			cacheDir.mkdirs();
		}
	}

	public File getFile(final String url) {
		//I identify images by hash code. Not a perfect solution, good for the demo.
		final String filename = String.valueOf(url.hashCode());
		final File f = new File(cacheDir, filename);
		return f;
	}

	public void clear() {
		final File[] files = cacheDir.listFiles();
		for (final File f : files) {
			f.delete();
		}
	}
}
