package org.informedindividual.util.lazylist;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import android.graphics.Bitmap;

public class MemoryCache {
	private final HashMap<String, SoftReference<Bitmap>> cache = new HashMap<String, SoftReference<Bitmap>>();

	public Bitmap get(final String id) {
		if (!cache.containsKey(id)) {
			return null;
		}
		final SoftReference<Bitmap> ref = cache.get(id);
		return ref.get();
	}

	public void put(final String id, final Bitmap bitmap) {
		cache.put(id, new SoftReference<Bitmap>(bitmap));
	}

	public void clear() {
		cache.clear();
	}
}
