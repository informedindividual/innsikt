package org.informedindividual.util.lazylist;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;
import java.util.WeakHashMap;
import javax.net.ssl.HttpsURLConnection;
import org.informedindividual.R;
import org.informedindividual.util.NetworkHelper;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

public class ImageLoader {
	private final int stub_id = R.drawable.stub;

	private final MemoryCache memoryCache = new MemoryCache();

	private final FileCache fileCache;

	private final Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());

	public ImageLoader(final Context context) {
		//Make the background thread low priority. This way it will not affect the UI performance
		photoLoaderThread.setPriority(Thread.NORM_PRIORITY - 1);
		fileCache = new FileCache(context);
	}

	public void DisplayImage(final String url, final Activity activity, final ImageView imageView) {
		imageViews.put(imageView, url);
		final Bitmap bitmap = memoryCache.get(url);
		if (bitmap != null) {
			imageView.setImageBitmap(bitmap);
		} else {
			queuePhoto(url, activity, imageView);
			imageView.setImageResource(stub_id);
		}
	}

	private void queuePhoto(final String url, final Activity activity, final ImageView imageView) {
		//This ImageView may be used for other images before. So there may be some old tasks in the queue. We need to discard them.
		photosQueue.clean(imageView);
		final PhotoToLoad p = new PhotoToLoad(url, imageView);
		synchronized (photosQueue.photosToLoad) {
			photosQueue.photosToLoad.offer(p);
			photosQueue.photosToLoad.notifyAll();
		}
		//start thread if it's not started yet
		if (photoLoaderThread.getState() == Thread.State.NEW) {
			photoLoaderThread.start();
		}
	}

	private Bitmap getBitmap(final String url) {
		final File f = fileCache.getFile(url);
		//from SD cache
		final Bitmap b = decodeFile(f);
		if (b != null) {
			return b;
		}
		//from web
		try {
			Bitmap bitmap = null;
			final URL imageUrl = new URL(url);
			HttpURLConnection conn = null;
			if (imageUrl.getProtocol().toLowerCase().equals("https")) {
				NetworkHelper.trustAllHosts();
				final HttpsURLConnection https = (HttpsURLConnection)imageUrl.openConnection();
				https.setHostnameVerifier(NetworkHelper.DO_NOT_VERIFY);
				conn = https;
			} else {
				conn = (HttpURLConnection)imageUrl.openConnection();
			}
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			final InputStream is = conn.getInputStream();
			final OutputStream os = new FileOutputStream(f);
			CopyStream(is, os);
			os.close();
			bitmap = decodeFile(f);
			return bitmap;
		} catch (final Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	private void CopyStream(final InputStream is, final OutputStream os) {
		final int buffer_size = 1024;
		try {
			final byte[] bytes = new byte[buffer_size];
			for (;;) {
				final int count = is.read(bytes, 0, buffer_size);
				if (count == -1) {
					break;
				}
				os.write(bytes, 0, count);
			}
		} catch (final Exception ex) {
		}
	}

	//decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile(final File f) {
		try {
			//decode image size
			final BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);
			//Find the correct scale value. It should be the power of 2.
			final int REQUIRED_SIZE = 70;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if ( (width_tmp / 2 < REQUIRED_SIZE) || (height_tmp / 2 < REQUIRED_SIZE)) {
					break;
				}
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}
			//decode with inSampleSize
			final BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (final FileNotFoundException e) {
		}
		return null;
	}

	//Task for the queue
	private class PhotoToLoad {
		public String url;

		public ImageView imageView;

		public PhotoToLoad(final String u, final ImageView i) {
			url = u;
			imageView = i;
		}
	}

	private final PhotosQueue photosQueue = new PhotosQueue();

	public void stopThread() {
		photoLoaderThread.interrupt();
	}

	//stores list of photos to download
	private class PhotosQueue {
		//FIFO list
		private final LinkedList<PhotoToLoad> photosToLoad = new LinkedList<PhotoToLoad>();

		//removes all instances of this ImageView
		public void clean(final ImageView image) {
			for (int j = 0; j < photosToLoad.size();) {
				if (photosToLoad.get(j).imageView == image) {
					photosToLoad.remove(j);
				} else {
					++j;
				}
			}
		}
	}
	private class PhotosLoader extends Thread {
		@Override
		public void run() {
			try {
				while (true) {
					//thread waits until there are any images to load in the queue
					if (photosQueue.photosToLoad.size() == 0) {
						synchronized (photosQueue.photosToLoad) {
							photosQueue.photosToLoad.wait();
						}
					}
					if (photosQueue.photosToLoad.size() != 0) {
						PhotoToLoad photoToLoad;
						synchronized (photosQueue.photosToLoad) {
							photoToLoad = photosQueue.photosToLoad.poll();
						}
						final Bitmap bmp = getBitmap(photoToLoad.url);
						memoryCache.put(photoToLoad.url, bmp);
						final String tag = imageViews.get(photoToLoad.imageView);
						if ( (tag != null) && tag.equals(photoToLoad.url)) {
							final BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad.imageView);
							final Activity a = (Activity)photoToLoad.imageView.getContext();
							a.runOnUiThread(bd);
						}
					}
					if (Thread.interrupted()) {
						break;
					}
				}
			} catch (final InterruptedException e) {
				//allow thread to exit
			}
		}
	}

	PhotosLoader photoLoaderThread = new PhotosLoader();

	//Used to display bitmap in the UI thread
	private class BitmapDisplayer implements Runnable {
		private final Bitmap bitmap;

		private final ImageView imageView;

		public BitmapDisplayer(final Bitmap b, final ImageView i) {
			bitmap = b;
			imageView = i;
		}

		@Override
		public void run() {
			if (bitmap != null) {
				imageView.setImageBitmap(bitmap);
			} else {
				imageView.setImageResource(stub_id);
			}
		}
	}

	public void clearCache() {
		memoryCache.clear();
		fileCache.clear();
	}
}
