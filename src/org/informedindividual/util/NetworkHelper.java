package org.informedindividual.util;

import java.io.IOException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkHelper {
	private final Context context;

	private final HttpClient httpClient;

	private final HttpContext httpContext;

	/*
	 * Always verify the host - don't check for certificate.
	 */
	public static final HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
		@Override
		public boolean verify(final String hostname, final SSLSession session) {
			return true;
		}
	};

	public NetworkHelper(final Context context) {
		super();
		this.context = context;
		httpClient = createHttpClient();
		httpContext = createHttpContext();
	}

	public boolean isOnline() {
		final ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
		if ( (netInfo != null) && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	private HttpContext createHttpContext() {
		// Create local HTTP context
		final HttpContext httpContext = new BasicHttpContext();
		// Create a local instance of cookie store
		final CookieStore cookieStore = new BasicCookieStore();
		// Bind custom cookie store to the local context
		httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
		return httpContext;
	}

	/*
	 * reuse this object to ensure that sessions are working. Trusting all certificates using HttpClient over HTTPS.
	 */
	private HttpClient createHttpClient() {
		try {
			final KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null, null);
			final SSLSocketFactory sf = new IISSLSocketFactory(trustStore);
			sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			final HttpParams params = new BasicHttpParams();
			HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
			HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
			final SchemeRegistry registry = new SchemeRegistry();
			registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			registry.register(new Scheme("https", sf, 443));
			final ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
			return new DefaultHttpClient(ccm, params);
		} catch (final Exception e) {
			return new DefaultHttpClient();
		}
	}

	/**
	 * Sends HTTP GET request and returns response from server
	 */
	public HttpResponse httpGet(final String urlToServer) throws IOException {
		final HttpGet request = new HttpGet(urlToServer);
		final HttpResponse response = httpClient.execute(request, httpContext);
		return response;
	}

	/**
	 * Sends HTTP GET request and returns response from server
	 */
	public HttpResponse httpGet(final String urlToServer, final List<BasicNameValuePair> parameterList) throws IOException {
		final String url = urlToServer + "?" + URLEncodedUtils.format(parameterList, null);
		final HttpGet request = new HttpGet(url);
		final HttpResponse response = httpClient.execute(request, httpContext);
		return response;
	}

	/**
	 * Sends HTTP POST request and returns response from server
	 */
	public HttpResponse httpPost(final String urlToServer, final List<BasicNameValuePair> parameterList) throws IOException {
		final HttpPost request = new HttpPost(urlToServer);
		final UrlEncodedFormEntity formParameters = new UrlEncodedFormEntity(parameterList);
		request.setEntity(formParameters);
		final HttpResponse response = httpClient.execute(request, httpContext);
		return response;
	}

	/**
	 * Sends HTTP GET request and returns HTTPResponse object. The response object contains HTTP headers and body.
	 */
	public HttpResponse httpGetResponse(String urlToServer, final List<BasicNameValuePair> parameterList) throws IOException {
		if (parameterList != null) {
			urlToServer = urlToServer + "?" + URLEncodedUtils.format(parameterList, null);
		}
		final HttpGet request = new HttpGet(urlToServer);
		final HttpResponse response = httpClient.execute(request, httpContext);
		return response;
	}

	/**
	 * Trust every server - don't check for any certificate.
	 */
	public static void trustAllHosts() {
		// Create a trust manager that does not validate certificate chains
		final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[]{};
			}

			@Override
			public void checkClientTrusted(final X509Certificate[] chain, final String authType) throws CertificateException {
			}

			@Override
			public void checkServerTrusted(final X509Certificate[] chain, final String authType) throws CertificateException {
			}
		}};
		// Install the all-trusting trust manager
		try {
			final SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Url should work with http://, https:// and www. Checks and make the url start with http or https
	 */
	public static String getCompleteUrl(final String url) {
		if (url.toLowerCase().trim().startsWith("http://") || url.toLowerCase().trim().startsWith("https://")) {
			return url;
		} else if (url.toLowerCase().trim().startsWith("www.")) {
			return "http://" + url;
		} else {
			return url;
		}
	}
}
