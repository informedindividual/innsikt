package org.informedindividual.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.informedindividual.model.Addative;
import org.informedindividual.model.Bootstrap;
import org.informedindividual.model.Filter;
import org.informedindividual.model.Ingredient;
import org.informedindividual.model.Opinion;
import org.informedindividual.model.Product;
import org.informedindividual.model.Settings;
import org.informedindividual.model.User;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import android.content.res.XmlResourceParser;

public class XmlFileParser {
	private static final String XML_TAG_LOGIN = "login";

	private static final String XML_TAG_LOGOUT = "logout";

	private static final String XML_TAG_CREATE = "create";

	private static final String XML_TAG_FILTERS = "filters";

	private static final String XML_TAG_FILTER = "filter";

	private static final String XML_TAG_ATTRIBUTE_URL = "url";
	
	private static final String XML_TAG_PRODUCT = "product";
	
	private static final String XML_TAG_RESULT = "result";

	private static final String XML_TAG_NAME = "name";

	private static final String XML_TAG_VENDOR = "vendor";

	private static final String XML_TAG_ATTRIBUTE_KEY = "key";

	private static final String XML_TAG_DESCRIPTION = "description";

	private static final String XML_TAG_IMAGE = "image";

	private static final String XML_TAG_OPINIONS = "opinions";

	private static final String XML_TAG_OPINION = "opinion";

	private static final String XML_TAG_INGREDIENTS = "ingredients";

	private static final String XML_TAG_INGREDIENT = "ingredient";

	private static final String XML_TAG_PRODUCT_NUTRIENTS = "product-nutrients";

	private static final String XML_TAG_ADDITIVES = "additives";

	private static final String XML_TAG_ADDATIVE = "additive";

	private static final String XML_TAG_SCORE = "score";

	private static final String XML_TAG_URL = "url";

	private static final String XML_TAG_CODE = "code";

	private static final String XML_TAG_TYPE = "type";

	private static final String XML_TAG_IMAGE_URL = "image-url";

	private static final String XML_TAG_USER = "user";

	private static final String XML_TAG_SETTINGS = "settings";

	private static final String XML_TAG_BARCODE_LOOKUP = "barcode-lookup";

	private static final String XML_TAG_EMAIL = "email";

	private static final String XML_TAG_EMAIL_EXISTS = "emailexists";

	private static final String XML_TAG_GROUPS = "groups";

	private static final String XML_TAG_ATTRIBUTE_SUBSCRIBED = "subscribed";

	private static final String XML_TAG_ATTRIBUTE_TOGGLE_SUBSCRIPTION = "toggle-subscription";

	private static final String XML_TAG_BARCODE = "barcode";

	private static final String XML_TAG_TARGET_TYPE = "target-type";

	private static final String XML_TAG_TARGET_NAME = "target-name";

	public Settings parseSettingsXml(final HttpResponse settingsXmlResponse) throws XmlPullParserException, IOException {
		final Settings settings = new Settings();
		final XmlPullParser xmlParser = XmlPullParserFactory.newInstance().newPullParser();
		final InputStream xmlInputStream = settingsXmlResponse.getEntity().getContent();
		final Reader xmlReader = new InputStreamReader(xmlInputStream);
		xmlParser.setInput(xmlReader);
		int eventType = -1;
		while (eventType != XmlResourceParser.END_DOCUMENT) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_FILTERS)) {
					settings.filters = parseSettingFiltersXml(xmlParser);
				}
			}
			eventType = xmlParser.next();
		}
		return settings;
	}

	private ArrayList<Filter> parseSettingFiltersXml(final XmlPullParser xmlParser) throws XmlPullParserException, IOException {
		final ArrayList<Filter> filters = new ArrayList<Filter>();
		boolean ferdig = false;
		int eventType = -1;
		while (!ferdig) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_FILTER)) {
					final String filterKey = xmlParser.getAttributeValue(null, XML_TAG_ATTRIBUTE_KEY);
					final String filterSubscribed = xmlParser.getAttributeValue(null, XML_TAG_ATTRIBUTE_SUBSCRIBED);
					final String filterToggleSubscriptionUrl = xmlParser.getAttributeValue(null, XML_TAG_ATTRIBUTE_TOGGLE_SUBSCRIPTION);
					final Filter filter = parseFilterXml(xmlParser);
					filter.key = filterKey;
					filter.subscribed = filterSubscribed;
					filter.toggleSubscriptionUrl = filterToggleSubscriptionUrl;
					filters.add(filter);
				}
			} else if (eventType == XmlResourceParser.END_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_FILTERS)) {
					ferdig = true;
				}
			}
			eventType = xmlParser.next();
		}
		return filters;
	}

	private ArrayList<Opinion> parseProductOpinonsXml(final XmlPullParser xmlParser) throws XmlPullParserException, IOException {
		final ArrayList<Opinion> opinions = new ArrayList<Opinion>();
		boolean ferdig = false;
		int eventType = -1;
		while (!ferdig) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_OPINION)) {
					final String key = xmlParser.getAttributeValue(null, XML_TAG_ATTRIBUTE_KEY);
					final Opinion opinion = parseProductOpinionXml(xmlParser);
					opinion.key = key;
					opinions.add(opinion);
				}
			} else if (eventType == XmlResourceParser.END_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_OPINIONS)) {
					ferdig = true;
				}
			}
			eventType = xmlParser.next();
		}
		return opinions;
	}

	private Opinion parseProductOpinionXml(final XmlPullParser xmlParser) throws XmlPullParserException, IOException {
		final Opinion opinion = new Opinion();
		boolean ferdig = false;
		int eventType = -1;
		while (!ferdig) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_TARGET_TYPE)) {
					opinion.targetType = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_TARGET_NAME)) {
					opinion.targetName = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_SCORE)) {
					opinion.score = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_NAME)) {
					opinion.name = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_IMAGE_URL)) {
					opinion.imageUrl = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_DESCRIPTION)) {
					opinion.description = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_URL)) {
					opinion.url = xmlParser.nextText().trim();
				}
			} else if (eventType == XmlResourceParser.END_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_OPINION)) {
					ferdig = true;
				}
			}
			eventType = xmlParser.next();
		}
		return opinion;
	}

	private Filter parseFilterXml(final XmlPullParser xmlParser) throws XmlPullParserException, IOException {
		final Filter filter = new Filter();
		boolean ferdig = false;
		int eventType = -1;
		while (!ferdig) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_NAME)) {
					filter.name = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_DESCRIPTION)) {
					filter.description = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_URL)) {
					filter.url = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_IMAGE_URL)) {
					filter.imageUrl = xmlParser.nextText().trim();
				}
			} else if (eventType == XmlResourceParser.END_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_FILTER)) {
					ferdig = true;
				}
			}
			eventType = xmlParser.next();
		}
		return filter;
	}

	private ArrayList<Ingredient> parseProductDetailIngredientsXml(final XmlPullParser xmlParser) throws XmlPullParserException, IOException {
		final ArrayList<Ingredient> ingredients = new ArrayList<Ingredient>();
		boolean ferdig = false;
		int eventType = -1;
		while (!ferdig) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_INGREDIENT)) {
					final Ingredient ingredient = parseProductDetailIngredientXml(xmlParser);
					ingredients.add(ingredient);
				}
			} else if (eventType == XmlResourceParser.END_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_INGREDIENTS)) {
					ferdig = true;
				}
			}
			eventType = xmlParser.next();
		}
		return ingredients;
	}

	private ArrayList<Addative> parseProductDetailAddativesXml(final XmlPullParser xmlParser) throws XmlPullParserException, IOException {
		final ArrayList<Addative> additives = new ArrayList<Addative>();
		boolean ferdig = false;
		int eventType = -1;
		while (!ferdig) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_ADDATIVE)) {
					final Addative addative = parseProductDetailAddativeXml(xmlParser);
					additives.add(addative);
				}
			} else if (eventType == XmlResourceParser.END_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_ADDITIVES)) {
					ferdig = true;
				}
			}
			eventType = xmlParser.next();
		}
		return additives;
	}

	private Addative parseProductDetailAddativeXml(final XmlPullParser xmlParser) throws XmlPullParserException, IOException {
		final Addative additive = new Addative();
		boolean ferdig = false;
		int eventType = -1;
		while (!ferdig) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_CODE)) {
					final String code = xmlParser.nextText().trim();
					additive.code = code;
				} else if (tagName.equals(XML_TAG_NAME)) {
					final String name = xmlParser.nextText().trim();
					additive.name = name;
				} else if (tagName.equals(XML_TAG_TYPE)) {
					final String type = xmlParser.nextText().trim();
					additive.type = type;
				}
			} else if (eventType == XmlResourceParser.END_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_ADDATIVE)) {
					ferdig = true;
				}
			}
			eventType = xmlParser.next();
		}
		return additive;
	}

	private Ingredient parseProductDetailIngredientXml(final XmlPullParser xmlParser) throws XmlPullParserException, IOException {
		final Ingredient ingredient = new Ingredient();
		boolean ferdig = false;
		int eventType = -1;
		while (!ferdig) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_NAME)) {
					final String ingredientName = xmlParser.nextText().trim();
					ingredient.name = ingredientName;
				}
			} else if (eventType == XmlResourceParser.END_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_INGREDIENT)) {
					ferdig = true;
				}
			}
			eventType = xmlParser.next();
		}
		return ingredient;
	}

	private Map<String, String> parseProductDetailNutrientsXml(final XmlPullParser xmlParser) throws XmlPullParserException, IOException {
		final Map<String, String> nutrients = new HashMap<String, String>();
		boolean ferdig = false;
		int eventType = -1;
		while (!ferdig) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				nutrients.put(tagName, xmlParser.nextText().trim());
			} else if (eventType == XmlResourceParser.END_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_PRODUCT_NUTRIENTS)) {
					ferdig = true;
				}
			}
			eventType = xmlParser.next();
		}
		return nutrients;
	}

	public Bootstrap parseBootstrapXml(final HttpResponse bootstrapXmlResponse) throws XmlPullParserException, IOException {
		final Bootstrap bootstrap = new Bootstrap();
		final XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		final XmlPullParser xmlParser = factory.newPullParser();
		final InputStream xmlInputStream = bootstrapXmlResponse.getEntity().getContent();
		final Reader xmlReader = new InputStreamReader(xmlInputStream);
		xmlParser.setInput(xmlReader);
		int eventType = -1;
		while (eventType != XmlResourceParser.END_DOCUMENT) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_LOGIN)) {
					bootstrap.loginUrl = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_CREATE)) {
					bootstrap.createUserUrl = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_EMAIL_EXISTS)) {
					bootstrap.emailExistsUrl = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_GROUPS)) {
					bootstrap.filters = parseFiltersXml(xmlParser);
				}
			}
			eventType = xmlParser.next();
		}
		return bootstrap;
	}

	public User parseUserLoginXml(final HttpResponse userLoginXmlResponse) throws XmlPullParserException, IOException {
		final User user = new User();
		final XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		final XmlPullParser xmlParser = factory.newPullParser();
		final InputStream xmlInputStream = userLoginXmlResponse.getEntity().getContent();
		final Reader xmlReader = new InputStreamReader(xmlInputStream);
		xmlParser.setInput(xmlReader);
		int eventType = -1;
		while (eventType != XmlResourceParser.END_DOCUMENT) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_USER)) {
					user.userKey = xmlParser.getAttributeValue(null, XML_TAG_ATTRIBUTE_KEY).trim();
				} else if (tagName.equals(XML_TAG_NAME)) {
					user.name = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_EMAIL)) {
					user.email = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_LOGOUT)) {
					user.logoutUrl = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_SETTINGS)) {
					user.settingsUrl = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_BARCODE_LOOKUP)) {
					user.barcodeLookupUrl = xmlParser.nextText().trim();
				}
			}
			eventType = xmlParser.next();
		}
		return user;
	}

	public boolean parseEmailExistsXml(final HttpResponse emailExistsXmlResponse) throws XmlPullParserException, IOException {
		boolean emailExists = false;
		final XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		final XmlPullParser xmlParser = factory.newPullParser();
		final InputStream xmlInputStream = emailExistsXmlResponse.getEntity().getContent();
		final Reader xmlReader = new InputStreamReader(xmlInputStream);
		xmlParser.setInput(xmlReader);
		int eventType = -1;
		while (eventType != XmlResourceParser.END_DOCUMENT) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_EMAIL_EXISTS)) {
					final String emailexistsString = xmlParser.nextText().trim();
					if (emailexistsString.equalsIgnoreCase("true")) {
						emailExists = true;
					}
				}
			}
			eventType = xmlParser.next();
		}
		return emailExists;
	}

	private ArrayList<Filter> parseFiltersXml(final XmlPullParser xmlParser) throws XmlPullParserException, IOException {
		final ArrayList<Filter> filters = new ArrayList<Filter>();
		boolean ferdig = false;
		int eventType = -1;
		while (!ferdig) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_FILTER)) {
					final String filterKey = xmlParser.getAttributeValue(null, XML_TAG_ATTRIBUTE_KEY);
					final Filter filter = parseFilterXml(xmlParser);
					filter.key = filterKey;
					filters.add(filter);
				}
			} else if (eventType == XmlResourceParser.END_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_FILTERS)) {
					ferdig = true;
				}
			}
			eventType = xmlParser.next();
		}
		return filters;
	}
	
	public List<Product> parseProductsOverviewXml(final HttpResponse productXmlResponse) throws XmlPullParserException, IOException {
		final ArrayList<Product> products = new ArrayList<Product>();		
		final XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		final XmlPullParser xmlParser = factory.newPullParser();
		final InputStream xmlInputStream = productXmlResponse.getEntity().getContent();
		final Reader xmlReader = new InputStreamReader(xmlInputStream);
		xmlParser.setInput(xmlReader);
		
		boolean ferdig = false;
		int eventType = -1;
		while (!ferdig) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_PRODUCT)) {
					products.add(parseProductXml(xmlParser));
				}
			} else if (eventType == XmlResourceParser.END_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_RESULT)) {
					ferdig = true;
				}
			}
			eventType = xmlParser.next();
		}
		return products;
	}
	
	private Product parseProductXml(final XmlPullParser xmlParser) throws XmlPullParserException, IOException {
		final Product product = new Product();
		boolean ferdig = false;
		int eventType = -1;
		while (!ferdig) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_BARCODE)) {
					product.barcode = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_NAME)) {
					if(product.name == null || product.name.equalsIgnoreCase("")){
						product.name = xmlParser.nextText().trim();
					}					
				} else if (tagName.equals(XML_TAG_VENDOR)) {
					product.vendor = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_IMAGE)) {
					product.imageUrl = xmlParser.getAttributeValue(null, XML_TAG_ATTRIBUTE_URL);
				} else if (tagName.equals(XML_TAG_OPINION)) {
					product.opinion = xmlParser.nextText().trim();
				}
				
			} else if (eventType == XmlResourceParser.END_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_PRODUCT)) {
					ferdig = true;
				}
			}
			eventType = xmlParser.next();
		}
		return product;
	}

	public Product parseProductOverviewXml(final HttpResponse productXmlResponse) throws XmlPullParserException, IOException {
		final Product product = new Product();
		final XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		final XmlPullParser xmlParser = factory.newPullParser();
		final InputStream xmlInputStream = productXmlResponse.getEntity().getContent();
		//String xmlString = IOUtil.inputStreamToString(xmlInputStream).toString(); //Used in debug mode to see how XML looks like
		final Reader xmlReader = new InputStreamReader(xmlInputStream);
		xmlParser.setInput(xmlReader);
		int eventType = -1;
		while (eventType != XmlResourceParser.END_DOCUMENT) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_BARCODE)) {
					product.barcode = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_NAME)) {
					if(product.name == null || product.name.equalsIgnoreCase("")){
						product.name = xmlParser.nextText().trim();
					}					
				} else if (tagName.equals(XML_TAG_VENDOR)) {
					product.vendor = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_IMAGE)) {
					product.imageUrl = xmlParser.getAttributeValue(null, XML_TAG_ATTRIBUTE_URL);
				} else if (tagName.equals(XML_TAG_OPINION)) {
					product.opinion = xmlParser.nextText().trim();
				}
			}
			eventType = xmlParser.next();
		}
		return product;
	}
	
	public Product parseProductDetailXml(final HttpResponse productDetailXmlResponse) throws XmlPullParserException, IOException {
		final Product product = new Product();
		final XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
		final XmlPullParser xmlParser = factory.newPullParser();
		final InputStream xmlInputStream = productDetailXmlResponse.getEntity().getContent();
		final Reader xmlReader = new InputStreamReader(xmlInputStream);
		xmlParser.setInput(xmlReader);
		int eventType = -1;
		while (eventType != XmlResourceParser.END_DOCUMENT) {
			if (eventType == XmlResourceParser.START_TAG) {
				final String tagName = xmlParser.getName();
				if (tagName.equals(XML_TAG_BARCODE)) {
					product.barcode = xmlParser.nextText().trim().trim();
				} else if (tagName.equals(XML_TAG_NAME)) {
					product.name = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_VENDOR)) {
					product.vendor = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_IMAGE)) {
					product.imageUrl = xmlParser.getAttributeValue(null, XML_TAG_ATTRIBUTE_URL);
				} else if (tagName.equals(XML_TAG_OPINION)) {
					product.opinion = xmlParser.nextText().trim();
				} else if (tagName.equals(XML_TAG_OPINIONS)) {
					product.opinions = parseProductOpinonsXml(xmlParser);
				} else if (tagName.equals(XML_TAG_INGREDIENTS)) {
					product.ingredients = parseProductDetailIngredientsXml(xmlParser);
				} else if (tagName.equals(XML_TAG_PRODUCT_NUTRIENTS)) {
					product.nutrients = parseProductDetailNutrientsXml(xmlParser);
				} else if (tagName.equals(XML_TAG_ADDITIVES)) {
					product.addatives = parseProductDetailAddativesXml(xmlParser);
				}
			}
			eventType = xmlParser.next();
		}
		return product;
	}
}
