package org.informedindividual.util;

import java.util.regex.Pattern;
import android.content.Context;
import android.text.InputFilter;
import android.text.Spanned;
import android.widget.EditText;

public class InputUtil {
	private final Context context;

	public InputUtil(final Context context) {
		this.context = context;
	}

	public void setIntegerInputFilter(final EditText... fields) {
		final IntegerInputFilter filter = new IntegerInputFilter();
		for (final EditText field : fields) {
			field.setFilters(new InputFilter[]{filter});
		}
	}

	public void setPriceInputFilter(final EditText... fields) {
		final PriceInputFilter filter = new PriceInputFilter();
		for (final EditText field : fields) {
			field.setFilters(new InputFilter[]{filter});
		}
	}

	private class IntegerInputFilter implements InputFilter {
		@Override
		public CharSequence filter(final CharSequence source, final int start, final int end, final Spanned dest, final int dstart, final int dend) {
			final String checkedText = dest.toString() + source.toString();
			final String pattern = getIntegerPattern();
			if (!Pattern.matches(pattern, checkedText)) {
				return "";
			}
			return null;
		}

		private String getIntegerPattern() {
			return "\\d+";
		}
	}
	private class PriceInputFilter implements InputFilter {
		@Override
		public CharSequence filter(final CharSequence source, final int start, final int end, final Spanned dest, final int dstart, final int dend) {
			final String checkedText = dest.toString() + source.toString();
			final String pattern = getPricePattern();
			if (!Pattern.matches(pattern, checkedText)) {
				return "";
			}
			return null;
		}

		private String getPricePattern() {
			return "[0-9]+([.]{1}||[.]{1}[0-9]{1,2})?";
		}
	}
}
