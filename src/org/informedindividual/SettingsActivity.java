package org.informedindividual;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.informedindividual.model.Filter;
import org.informedindividual.model.Settings;
import org.informedindividual.util.NetworkHelper;
import org.informedindividual.util.XmlFileParser;
import org.informedindividual.zxing.camera.CameraManager;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

public class SettingsActivity extends Activity {
	private IIApplication iiApplication;

	private SharedPreferences iiSharedPreferences;
	
	private TextView settingsTitle;

	private Settings settings;

	final XmlFileParser xmlFileParser = new XmlFileParser();

	private final MessageManager messageManager = new MessageManager(this);

	private FilterAdapter filterAdapter;

	private ListView filterList;

	private Button saveChangesButton;

	private Dialog loadingDialog;

	private Button logoutButton;

	private final Handler settingsHandler = new Handler();

	private final Runnable updateResults = new Runnable() {
		@Override
		public void run() {
			filterList = (ListView)findViewById(R.id.filters_list);
			filterAdapter = new FilterAdapter(SettingsActivity.this, R.layout.settings_filter_item, settings.filters);
			setListHeaderAndFooter(settings);
			filterList.setAdapter(filterAdapter);
			loadingDialog.dismiss();
			//First time login on this device
			//userFirstTimeLogin is default true, and set to false when user clicks on OK button in the info dialog.
			final boolean userFirstTimeLogin = iiSharedPreferences.getBoolean(IIPreferences.USER_FIRST_TIME_LOGIN, true);
			if (userFirstTimeLogin) {
				showFirstTimeLoginInfoDialogWindow();
			}
		}
	};
	/*
	private void fetchSetingsDataFromServer() {
		final Thread thread = new Thread() {
			@Override
			public void run() {
				try {
					//Get settings XML data
					final String userSettingsUrl = iiSharedPreferences.getString(IIPreferences.USER_SETTINGS_URL, "");
					final HttpResponse setingsXmlResponse = iiApplication.network.httpGet(userSettingsUrl);
					settings = xmlFileParser.parseSettingsXml(setingsXmlResponse);
				} catch (final IOException e) {
					messageManager.showNetworkErrorMessage(SettingsActivity.this);
				} catch (final XmlPullParserException e) {
					messageManager.showXmlParsingErrorMessage(SettingsActivity.this);
				}
				settingsHandler.post(updateResults);
			}
		};
		thread.start();
	}
	*/
	private void fetchSetingsDataFromServerWithoutLogin() {
		final Thread thread = new Thread() {
			@Override
			public void run() {
				try {
					//Get settings XML data
					final String settingsUrl = Configuration.getSettingsUrl();
					final HttpResponse setingsXmlResponse = iiApplication.network.httpGet(settingsUrl);
					settings = xmlFileParser.parseSettingsXml(setingsXmlResponse);
				} catch (final IOException e) {
					messageManager.showNetworkErrorMessage(SettingsActivity.this);
				} catch (final XmlPullParserException e) {
					messageManager.showXmlParsingErrorMessage(SettingsActivity.this);
				}
				settingsHandler.post(updateResults);
			}
		};
		thread.start();
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		iiApplication = (IIApplication)this.getApplication();
		iiSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		loadingDialog = new Dialog(this, R.style.FullHeightDialog);
		loadingDialog.setContentView(R.layout.loading_layout);
		loadingDialog.setCancelable(true);
		loadingDialog.show();
		//fetchSetingsDataFromServer();
		fetchSetingsDataFromServerWithoutLogin();
	}

	@Override
	public void onResume() {
		super.onResume();		
	}

	private void showFirstTimeLoginInfoDialogWindow() {
		final Dialog dialog = new Dialog(SettingsActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(true);
		dialog.setContentView(R.layout.first_time_login_info);
		final Button button = (Button)dialog.findViewById(R.id.Ok);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				final SharedPreferences.Editor iiPreferencesEditor = iiSharedPreferences.edit();
				iiPreferencesEditor.putBoolean(IIPreferences.USER_FIRST_TIME_LOGIN, false);
				iiPreferencesEditor.commit();
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	private void setListHeaderAndFooter(final Settings settings) {
		final LayoutInflater inflater = LayoutInflater.from(this);
		final View settingsTopView = inflater.inflate(R.layout.settings_view_top, filterList, false);
		settingsTitle = (TextView)settingsTopView.findViewById(R.id.settings_title);
		setTextToIndicateTestOrProd();
		/*
		final TextView emailTextView = (TextView)settingsTopView.findViewById(R.id.user_email);
		emailTextView.setText(iiSharedPreferences.getString(IIPreferences.USER_EMAIL, ""));
		logoutButton = (Button)settingsTopView.findViewById(R.id.logout_btn);
		logoutButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				logout();
			}
		});
		*/
		final View settingsBottomView = inflater.inflate(R.layout.settings_view_bottom, filterList, false);
		saveChangesButton = (Button)settingsBottomView.findViewById(R.id.save_changes_btn);
		saveChangesButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {				
				final Intent intent = new Intent(SettingsActivity.this, CaptureActivity.class);
				intent.putExtra(CaptureActivity.FILTERS_CHANGED_KEY, true);
				startActivity(intent);
				finish();
			}
		});
		filterList.addHeaderView(settingsTopView);
		filterList.addFooterView(settingsBottomView);
	}

	private void logout() {
		final String logoutUrl = iiSharedPreferences.getString(IIPreferences.USER_LOGOUT_URL, "");
		final String email = iiSharedPreferences.getString(IIPreferences.USER_EMAIL, "");
		final String password = iiSharedPreferences.getString(IIPreferences.USER_PASSWORD, "");
		final List<BasicNameValuePair> loginParams = new ArrayList<BasicNameValuePair>();
		final BasicNameValuePair emailParam = new BasicNameValuePair("email", email);
		final BasicNameValuePair passwordParam = new BasicNameValuePair("password", password);
		loginParams.add(emailParam);
		loginParams.add(passwordParam);
		try {
			iiApplication.network.httpPost(logoutUrl, loginParams);
			iiApplication.network = new NetworkHelper(this);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		final SharedPreferences.Editor iiPreferencesEditor = iiSharedPreferences.edit();
		iiPreferencesEditor.putBoolean(IIPreferences.USER_SIGNED_IN, false);
		iiPreferencesEditor.commit();
		finish();
		//Fix the problem with camera initialization error when login in again.
		CameraManager.get().stopPreview();
		CameraManager.get().closeDriver();
		iiApplication.captureActivity.finish();
		final Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
		startActivity(intent);
	}
	
	private void setTextToIndicateTestOrProd() {
		if(Configuration.SERVER_BASE_URL.equals(Configuration.SERVER_BASE_URL_TEST)){
			settingsTitle.setText(getString(R.string.settings_test));
		} else if(Configuration.SERVER_BASE_URL.equals(Configuration.SERVER_BASE_URL_PROD)){
			settingsTitle.setText(getString(R.string.settings));
		}
	}

	/**
	 * Adapter to display filter subscriptions in list.
	 */
	private class FilterAdapter extends ArrayAdapter<Filter> {
		private final Context context;

		private final ArrayList<Filter> filters;

		public FilterAdapter(final Context context, final int textViewResourceId, final ArrayList<Filter> filters) {
			super(context, textViewResourceId, filters);
			this.filters = filters;
			this.context = context;
		}

		@Override
		public View getView(final int position, final View convertView, final ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				final LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = layoutInflater.inflate(R.layout.settings_filter_item, null);
			}
			final Filter filter = filters.get(position);
			if (filter != null) {
				final CheckBox subscriptionCheckBox = (CheckBox)view.findViewById(R.id.subscription_checkbox);
				subscriptionCheckBox.setChecked( (filter.subscribed != null) && filter.subscribed.equalsIgnoreCase("true"));
				String selectedFilterKeys = iiSharedPreferences.getString(IIPreferences.USER_FILTERS, "");			
				subscriptionCheckBox.setChecked(selectedFilterKeys.contains(filter.key.trim()));				
				
				subscriptionCheckBox.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(final View view) {
						String selectedFilterKeys = iiSharedPreferences.getString(IIPreferences.USER_FILTERS, "");
						String[] keysArray = selectedFilterKeys.split(",");						
						ArrayList<String> keysList = new ArrayList<String>(Arrays.asList(keysArray));
						keysList.remove("");						
						if(subscriptionCheckBox.isChecked()){				
							if(!keysList.contains(filter.key.trim())){
								keysList.add(filter.key.trim());								
							}
						}else{
							keysList.remove(filter.key.trim());
						}			
						//
						if(keysList.size() == 0){
							selectedFilterKeys = "";
						}else if(keysList.size() == 1){
							selectedFilterKeys = keysList.get(0);													
						}else if(keysList.size() > 1){
							selectedFilterKeys = TextUtils.join(",", keysList);	
						}
						//
						final SharedPreferences.Editor iiPreferencesEditor = iiSharedPreferences.edit();
						iiPreferencesEditor.putString(IIPreferences.USER_FILTERS, selectedFilterKeys);
						iiPreferencesEditor.commit();
						/*
						try {
							if (filter.toggleSubscriptionUrl != null) {
								iiApplication.network.httpGet(filter.toggleSubscriptionUrl);
							}
						} catch (final IOException e) {
							e.printStackTrace();
						}*/	
					}
				});
				final TextView subscriptionNameTextView = (TextView)view.findViewById(R.id.filter_subscription_name);
				subscriptionNameTextView.setText(filter.name);
				final TextView subscriptionDesctiptionTextView = (TextView)view.findViewById(R.id.filter_subscription_description);
				subscriptionDesctiptionTextView.setText(getShortDescription(filter.description));
				//
				final Button detailButton = (Button)view.findViewById(R.id.settings_details_btn);
				detailButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(final View view) {
						final Intent intent = new Intent(SettingsActivity.this, SettingsFilterDetailActvity.class);
						intent.putExtra(SettingsFilterDetailActvity.FILTER, filter);
						startActivity(intent);
					}
				});
			}
			return view;
		}

		private String getShortDescription(final String description) {
			final int SHORT_DESCRIPTION_LENGTH = 67;
			if (description.length() > SHORT_DESCRIPTION_LENGTH) {
				return description.substring(0, SHORT_DESCRIPTION_LENGTH) + "...";
			} else {
				return description;
			}
		}
	}
}
