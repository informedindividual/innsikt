package org.informedindividual;

/**
 * Use http and not https for getting non-sensitive data for better response time.
 */	
public class Configuration {
	static String SERVER_BASE_URL_PROD = "http://informedindividual.org";
	static String SERVER_BASE_URL_TEST = "http://test.informedindividual.org";
	
	public static String SERVER_BASE_URL = SERVER_BASE_URL_PROD;		
	
	static String getServeBaseUrl(){
		return SERVER_BASE_URL;
				
	}
	
	static String getBootstrapUrl(){
		return getServeBaseUrl() + "/innsikt/api/bootstrap";
	}
	
	static String getUserbarcodeLookupUrl(){
		return getServeBaseUrl() + "/innsikt/api/lookup?detail=&barcode=";
	}
	
	static String getSettingsUrl(){
		return getServeBaseUrl() + "/innsikt/api/settings";
	}
	
}
