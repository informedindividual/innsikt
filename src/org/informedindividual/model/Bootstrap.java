package org.informedindividual.model;

import java.util.ArrayList;

public class Bootstrap {
	public String loginUrl;

	public String logoutUrl;

	public String createUserUrl;

	public String emailExistsUrl;

	public ArrayList<Filter> filters;

	public Bootstrap() {
		super();
	}

	public Bootstrap(final String loginUrl, final String logoutUrl, final String createUserUrl, final ArrayList<Filter> filters) {
		super();
		this.loginUrl = loginUrl;
		this.logoutUrl = logoutUrl;
		this.createUserUrl = createUserUrl;
		this.filters = filters;
	}
}
