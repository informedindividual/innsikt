package org.informedindividual.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Opinion implements Parcelable {
	public String key;

	public String targetType;

	public String targetName;

	public String score;

	public String name;

	public String imageUrl;

	public String description;

	public String url;

	public Opinion() {
		super();
	}

	public Opinion(final Parcel in) {
		this.key = in.readString();
		this.targetType = in.readString();
		this.targetName = in.readString();
		this.score = in.readString();
		this.name = in.readString();
		this.imageUrl = in.readString();
		this.description = in.readString();
		this.url = in.readString();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flag) {
		dest.writeString(key);
		dest.writeString(targetType);
		dest.writeString(targetName);
		dest.writeString(score);
		dest.writeString(name);
		dest.writeString(imageUrl);
		dest.writeString(description);
		dest.writeString(url);
	}

	@SuppressWarnings("rawtypes")
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		@Override
		public Opinion createFromParcel(final Parcel in) {
			return new Opinion(in);
		}

		@Override
		public Opinion[] newArray(final int size) {
			return new Opinion[size];
		}
	};
}
