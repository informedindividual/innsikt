package org.informedindividual.model;

public class User {
	public String userKey;

	public String name;

	public String email;

	public String logoutUrl;

	public String settingsUrl;

	public String barcodeLookupUrl;

	public User() {
		super();
	}

	public User(final String email) {
		super();
		this.email = email;
	}
}
