package org.informedindividual.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Filter implements Parcelable {
	public String key;

	public String subscribed;

	public String toggleSubscriptionUrl;

	public String name;

	public String description;

	public String url;

	public String imageUrl;

	public Filter() {
		super();
	}

	public Filter(final String key, final String subscribed, final String toggleSubscriptionUrl, final String name, final String description, final String url, final String imageUrl) {
		super();
		this.key = key;
		this.subscribed = subscribed;
		this.toggleSubscriptionUrl = toggleSubscriptionUrl;
		this.name = name;
		this.description = description;
		this.url = url;
		this.imageUrl = imageUrl;
	}

	public Filter(final Parcel in) {
		this.key = in.readString();
		this.subscribed = in.readString();
		this.toggleSubscriptionUrl = in.readString();
		this.name = in.readString();
		this.description = in.readString();
		this.url = in.readString();
		this.imageUrl = in.readString();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flag) {
		dest.writeString(key);
		dest.writeString(subscribed);
		dest.writeString(toggleSubscriptionUrl);
		dest.writeString(name);
		dest.writeString(description);
		dest.writeString(url);
		dest.writeString(imageUrl);
	}

	@SuppressWarnings("rawtypes")
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		@Override
		public Filter createFromParcel(final Parcel in) {
			return new Filter(in);
		}

		@Override
		public Filter[] newArray(final int size) {
			return new Filter[size];
		}
	};
}
