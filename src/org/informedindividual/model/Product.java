package org.informedindividual.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Product {
	public String barcode;

	public String name;

	public String imageUrl = "";

	public String vendor;

	public String opinion;

	public ArrayList<Opinion> opinions;

	public ArrayList<Ingredient> ingredients;

	public Map<String, String> nutrients = new HashMap<String, String>();

	public ArrayList<Addative> addatives;

	public Product() {
		super();
	}

	public Product(final String name) {
		super();
		this.name = name;
	}

	public Product(final String name, final String productImage) {
		super();
		this.name = name;
		this.imageUrl = productImage;
	}
}
