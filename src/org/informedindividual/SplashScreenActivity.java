package org.informedindividual;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashScreenActivity extends Activity {
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		final Thread splashThread = new Thread() {
			@Override
			public void run() {
				try {
					sleep(500);
					finish();
					final Intent intent = new Intent(SplashScreenActivity.this, CaptureActivity.class);
					startActivity(intent);
				} catch (final InterruptedException e) {
					// do nothing
				}
			}
		};
		splashThread.start();
	}
}
