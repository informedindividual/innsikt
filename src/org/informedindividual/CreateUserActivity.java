package org.informedindividual;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.informedindividual.util.XmlFileParser;
import org.xmlpull.v1.XmlPullParserException;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CreateUserActivity extends Activity {
	public static final String EMAIL_EXISTS_URL = "emailExistsUrl";

	public static final String CREATE_USER_URL = "createUserUrl";

	public static final String USER_LOGIN_URL = "userLoginUrl";

	public static final String EMAIL = "email";

	public static final String PASSWORD = "password";

	public static final String FILTERS = "filters";

	private String emailExistsUrl;

	private String createUserUrl;

	private IIApplication iiApplication;

	final private XmlFileParser xmlFileParser = new XmlFileParser();

	private final MessageManager messageManager = new MessageManager(this);

	private EditText emailEditText;

	private EditText passwordEditText;

	private EditText confirmPasswordEditText;

	private Button createUserButton;

	private Dialog loadingDialog;

	private TextView loaddingMessageTextView;

	private final Handler handler = new Handler();

	private final Runnable updateResults = new Runnable() {
		@Override
		public void run() {
			loadingDialog.dismiss();
		}
	};

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.create_user);
		iiApplication = (IIApplication)this.getApplication();
		emailExistsUrl = getIntent().getStringExtra(EMAIL_EXISTS_URL);
		createUserUrl = getIntent().getStringExtra(CREATE_USER_URL);
		emailEditText = (EditText)findViewById(R.id.email_edittxt);
		passwordEditText = (EditText)findViewById(R.id.password_edittxt);
		confirmPasswordEditText = (EditText)findViewById(R.id.confirm_password_edittxt);
		emailEditText.setText(getIntent().getStringExtra(EMAIL));
		passwordEditText.setText(getIntent().getStringExtra(PASSWORD));
		initializeCreateUserButton();
	}

	private void initializeCreateUserButton() {
		createUserButton = (Button)findViewById(R.id.create_user_btn);
		createUserButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(final View view) {
				final String email = emailEditText.getText().toString();
				final String password = passwordEditText.getText().toString();
				final String confirmPassword = confirmPasswordEditText.getText().toString();
				if (email.equalsIgnoreCase("")) {
					messageManager.showToastMessageShort(getString(R.string.email_required));
				} else if (password.equalsIgnoreCase("")) {
					messageManager.showToastMessageShort(getString(R.string.password_required));
				} else if (confirmPassword.equalsIgnoreCase("") || !password.equalsIgnoreCase(confirmPassword)) {
					messageManager.showToastMessageShort(getString(R.string.confirm_password_failed));
				} else {
					final String checkIfEmailExistsUrl = emailExistsUrl + email;
					boolean emailExists = false;
					try {
						final HttpResponse emailExistsXmlResponse = iiApplication.network.httpGetResponse(checkIfEmailExistsUrl, null);
						emailExists = xmlFileParser.parseEmailExistsXml(emailExistsXmlResponse);
						if (emailExists) {
							messageManager.showToastMessageShort(getString(R.string.email_exists));
						} else {
							initializeCreatingUserMessage();
							creatingNewUser();
						}
					} catch (final XmlPullParserException e) {
						messageManager.showCreateUserFailedToastMessage();
					} catch (final IOException e) {
						messageManager.showCreateUserFailedToastMessage();
					}
				}
			}
		});
	}

	private void creatingNewUser() {
		final Thread thread = new Thread() {
			@Override
			public void run() {
				final String email = emailEditText.getText().toString();
				final String password = passwordEditText.getText().toString();
				try {
					final List<BasicNameValuePair> createUserParams = new ArrayList<BasicNameValuePair>();
					final BasicNameValuePair emailParam = new BasicNameValuePair("email", email);
					final BasicNameValuePair usernameParam = new BasicNameValuePair("username", email);
					final BasicNameValuePair passwordParam = new BasicNameValuePair("password", password);
					createUserParams.add(emailParam);
					createUserParams.add(usernameParam);
					createUserParams.add(passwordParam);
					//
					String modifiedCreateUserUrl = createUserUrl.replace("&email=", "").replace("&username=", "").replace("&password=", "");
					//prevent Caused by: org.apache.http.client.CircularRedirectException: Circular redirect to 'http://informedindividual.org/ii/api/bootstrap'
					modifiedCreateUserUrl = modifiedCreateUserUrl.replace("&_redirect=" +  Configuration.getBootstrapUrl(), "");
					final HttpResponse createUserResponse = iiApplication.network.httpPost(modifiedCreateUserUrl, createUserParams);
					if (createUserResponse != null) {
						finish();
						final Intent intent = new Intent(CreateUserActivity.this, LoginActivity.class);
						intent.putExtra(CreateUserActivity.EMAIL, email);
						intent.putExtra(CreateUserActivity.PASSWORD, password);
						startActivity(intent);
					} else {
						//creating user failed
						messageManager.showCreateUserFailedToastMessage();
					}
				} catch (final Exception e) {
					//creating user failed
					messageManager.showCreateUserFailedToastMessage();
				}
				handler.post(updateResults);
			}
		};
		thread.start();
	}

	private void initializeCreatingUserMessage() {
		loadingDialog = new Dialog(this, R.style.FullHeightDialog);
		loadingDialog.setContentView(R.layout.loading_layout);
		loaddingMessageTextView = (TextView)loadingDialog.findViewById(R.id.loading_message);
		loaddingMessageTextView.setText(R.string.creating_user);
		loadingDialog.setCancelable(true);
		loadingDialog.show();
	}
}
