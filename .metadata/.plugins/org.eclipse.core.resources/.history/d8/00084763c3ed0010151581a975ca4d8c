package org.informedindividual.app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import org.informedindividual.app.model.Ingredient;
import org.informedindividual.app.model.Opinion;
import org.informedindividual.app.model.Product;
import org.informedindividual.app.util.XmlFileParser;
import org.informedindividual.app.util.lazylist.ImageLoader;
import org.xmlpull.v1.XmlPullParserException;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.util.Linkify;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class ProductDetailActivity extends Activity {
	private static final int UNKNOWN_PRODUCT_NOT_RATED_SCORE = 0;
	
	private final String UNIT_GRAMS = "grams";
	
	private final String UNIT_KCAL = "kcal";

	private final String ENERGI = "energy";

	private final String ENERGI_KCAL = "energy-kcal";

	private final String FAT = "fat";

	private final String PROTEIN = "protein";

	private final String CARBOHYDRATES = "carbohydrates";

	public static String BARCODE = "barcode";

	private IIApplication iiApplication;

	private LayoutInflater inflater;

	final XmlFileParser xmlFileParser = new XmlFileParser();

	private final MessageManager messageManager = new MessageManager(this);

	private Product product;

	private ImageLoader imageLoader;

	private LinearLayout productDetailLayout;

	private Dialog loadingDialog;

	private ImageView productImage;

	private TextView productNameTextView;

	private TextView productVendorTextView;

	private String barcode;

	private final Handler handler = new Handler();

	private final Runnable updateResults = new Runnable() {
		@Override
		public void run() {
			imageLoader.DisplayImage(product.getImageUrl(), ProductDetailActivity.this, productImage);
			productNameTextView.setText(product.getName());
			productVendorTextView.setText(product.getVendor());
			//
			final View ingredientsView = inflater.inflate(R.layout.product_detail_ingredients, null);
			final TextView ingredientsTextview = (TextView)ingredientsView.findViewById(R.id.ingredients);
			String ingredientText = "";
			final ArrayList<Ingredient> ingredients =  product.getIngredients();
			for (final Ingredient ingredient : product.getIngredients()) {
				ingredientText = ingredientText + ingredient.getName() + "\n";
			}
			ingredientsTextview.setText(ingredientText);
			productDetailLayout.addView(ingredientsView);
			createNutritionalContentGui();
			setOpinionTotalValueAndColor();
			for (final Opinion opinion : product.getOpinions()) {
				createOpinionGui(opinion);
			}
			loadingDialog.dismiss();
		}

		private void createNutritionalContentGui() {
			final View nutritionView = inflater.inflate(R.layout.product_detail_nutrition, null);
			final TextView nutritionTextview = (TextView)nutritionView.findViewById(R.id.nutrition_text);
			final TextView nutritionContentTextview = (TextView)nutritionView.findViewById(R.id.nutrition_content);
			final String nutritionText = titleCase(ENERGI) + "\n" + titleCase(FAT) + "\n" + titleCase(PROTEIN) + "\n" + titleCase(CARBOHYDRATES);
			final String nutritionContentText = ": " + product.getNutrients().get(ENERGI_KCAL) + " " + UNIT_KCAL + "\n" + ": " + product.getNutrients().get(FAT) + " " + UNIT_GRAMS + "\n"
					+ ": " + product.getNutrients().get(PROTEIN) + " " + UNIT_GRAMS + "\n" + ": " + product.getNutrients().get(CARBOHYDRATES) + " " + UNIT_GRAMS;
			final Button moreNutritionButton = (Button)nutritionView.findViewById(R.id.nutrition_details_btn);
			moreNutritionButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(final View view) {
					final Dialog dialog = new Dialog(ProductDetailActivity.this);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.setCancelable(true);
					dialog.setContentView(R.layout.product_detail_nutrition_more);
					final TextView nutritionMoreTextView = (TextView)dialog.findViewById(R.id.nutrition_textview);
					String nutritionText = "";
					for (final Map.Entry<String, String> entry : product.getNutrients().entrySet()) {
						nutritionText = nutritionText + "\n" + "   " + entry.getKey() + " " + entry.getValue();
					}
					nutritionText = nutritionText + "                                                                                  ";
					nutritionMoreTextView.setText(nutritionText);
					final Button button = (Button)dialog.findViewById(R.id.aboutOk);
					button.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(final View view) {
							dialog.dismiss();
						}
					});
					dialog.show();
				}
			});
			nutritionTextview.setText(nutritionText);
			nutritionContentTextview.setText(nutritionContentText);
			productDetailLayout.addView(nutritionView);
		}

		private String titleCase(final String text) {
			return text.substring(0, 1).toUpperCase() + text.substring(1, text.length()).toLowerCase();
		}

		private void createOpinionGui(final Opinion opinion) {
			final View opinionView = inflater.inflate(R.layout.product_detail_opinion, null);
			final LinearLayout scoreLayout = (LinearLayout)opinionView.findViewById(R.id.score_layout);
			final TextView opinionNameTextview = (TextView)opinionView.findViewById(R.id.opinion_name_textview);
			opinionNameTextview.setText(opinion.getName());
			final TextView opinionScoreTextview = (TextView)opinionView.findViewById(R.id.score_txtview);
			opinionScoreTextview.setText(getString(R.string.rates_as) + " " + opinion.getScore() + "%");
			//
			final Button detailButton = new Button(ProductDetailActivity.this);
			detailButton.setGravity(Gravity.CENTER);
			detailButton.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 56));
			detailButton.setText(getString(R.string.detail));
			detailButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(final View view) {
					final Dialog dialog = new Dialog(ProductDetailActivity.this);
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.setCancelable(true);
					dialog.setContentView(R.layout.product_detail_opinion_detail);
					final ImageView opinionImageView = (ImageView)dialog.findViewById(R.id.opinion_image);
					imageLoader.DisplayImage(product.getImageUrl(), ProductDetailActivity.this, opinionImageView);
					final TextView detailTextView = (TextView)dialog.findViewById(R.id.detail_textview);
					final String detailText = opinion.getDescription() + "\n";
					final TextView readmoreTextView = (TextView)dialog.findViewById(R.id.readmore_textview);
					final SpannableString s = new SpannableString(opinion.getUrl());
					Linkify.addLinks(s, Linkify.WEB_URLS);
					readmoreTextView.setText(s);
					detailTextView.setText(detailText);
					final Button button = (Button)dialog.findViewById(R.id.aboutOk);
					button.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(final View view) {
							dialog.dismiss();
						}
					});
					dialog.show();
				}
			});
			scoreLayout.addView(detailButton);
			productDetailLayout.addView(opinionView);
		}
	};

	private void fetchProductDetailDataFromServer() {
		final Thread thread = new Thread() {
			@Override
			public void run() {
				try {
					//Get product detail XML data
					final String productDetailUrl = Configuration.URL_BARCODE_LOOKUP + barcode
							+ "&detail=view3&userKey=692B0F1BED9ED9892C67898501AAC5DA13A48F6D";
					final String productDetailXml = iiApplication.getNetwork().httpGet(productDetailUrl);
					product = xmlFileParser.parseProductDetailXml(productDetailXml);
				} catch (final IOException e) {
					messageManager.showNetworkErrorMessage(ProductDetailActivity.this);
				} catch (final XmlPullParserException e) {
					messageManager.showXmlParsingErrorMessage(ProductDetailActivity.this);
				}
				handler.post(updateResults);
			}
		};
		thread.start();
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_detail);
		inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		iiApplication = (IIApplication)this.getApplication();
		barcode = getIntent().getStringExtra(BARCODE);
		imageLoader = new ImageLoader(this);
		productDetailLayout = (LinearLayout)findViewById(R.id.product_detail_layout);
		productImage = (ImageView)findViewById(R.id.product_detail_image);
		productNameTextView = (TextView)findViewById(R.id.product_name);
		productVendorTextView = (TextView)findViewById(R.id.product_vendor);
		loadingDialog = new Dialog(this, R.style.FullHeightDialog);
		loadingDialog.setContentView(R.layout.loading_layout);
		loadingDialog.setCancelable(true);
		loadingDialog.show();
		fetchProductDetailDataFromServer();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	private void setOpinionTotalValueAndColor() {
		final int score = Integer.parseInt(product.getOpinion());
		final View opinionTotalView = inflater.inflate(R.layout.product_detail_opinion_total, null);
		final LinearLayout scoreLayout = (LinearLayout)opinionTotalView.findViewById(R.id.scoreBackgroundLayout);
		if (score == UNKNOWN_PRODUCT_NOT_RATED_SCORE) {
			scoreLayout.setVisibility(View.INVISIBLE);
		} else {
			scoreLayout.setVisibility(View.VISIBLE);
		}
		final TextView scoreTextView = (TextView)opinionTotalView.findViewById(R.id.score_txtview);
		scoreTextView.setText(product.getOpinion() + "%");
		setOpinionColor(score, scoreLayout, scoreTextView);
		productDetailLayout.addView(opinionTotalView);
	}

	private void setOpinionColor(final int score, final LinearLayout scoreLayout, final TextView scoreTextView) {
		if (score <= 30) {
			//0-30 is displayed as a red number in a red circle.
			scoreLayout.setBackgroundResource(R.drawable.circle_red);
			scoreTextView.setTextColor(getResources().getColor(R.color.red));
		} else if ( (score >= 31) && (score <= 70)) {
			//31-70 is displayed as a black number in a black circle.
			scoreLayout.setBackgroundResource(R.drawable.circle_black);
			scoreTextView.setTextColor(getResources().getColor(R.color.black));
		} else if ( (score >= 71) && (score <= 100)) {
			//71-100 is displayed as a green number in a green circle.
			scoreLayout.setBackgroundResource(R.drawable.circle_green);
			scoreTextView.setTextColor(getResources().getColor(R.color.green));
		}
	}
}
